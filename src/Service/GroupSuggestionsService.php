<?php

namespace App\Service;

use App\Entity\Group;
use App\Entity\GroupSuggestion;
use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\GroupSuggestionManager;
use Doctrine\Common\Collections\ArrayCollection;

class GroupSuggestionsService
{
    private GroupManager $groupManager;

    private GroupSuggestionManager $groupSuggestionManager;

    public function __construct(GroupManager $groupManager, GroupSuggestionManager $groupSuggestionManager)
    {
        $this->groupManager = $groupManager;
        $this->groupSuggestionManager = $groupSuggestionManager;
    }

    private function makeGroupSuggestion(User $user, Group $group, float $percentLearnSkills): ?GroupSuggestion
    {
        $userSkills = new ArrayCollection($user->getSkills()->toArray());
        $groupSkills = $group->getSkills();

        $learnedSkills = new ArrayCollection();
        $additionalLearnedSkills = new ArrayCollection();

        foreach ($groupSkills as $skill) {
            if ($user->getSkills()->contains($skill)) {
                $learnedSkills->add($skill);
                $userSkills->removeElement($skill);
            } else {
                $additionalLearnedSkills->add($skill);
            }
        }

        $notLearnedSkills = $userSkills;

        if (($learnedSkills->count() / $user->getSkills()->count()) * 100 >= $percentLearnSkills ) {
            $groupSuggestion = new GroupSuggestion();

            $groupSuggestion->setGroup($group);
            $groupSuggestion->setLearnedSkills($learnedSkills);
            $groupSuggestion->setNotLearnedSkills($notLearnedSkills);
            $groupSuggestion->setAdditionalLearnedSkills($additionalLearnedSkills);
            $groupSuggestion->setStudent($user);

            return $groupSuggestion;
        }

        return null;
    }

    public function makeGroupSuggestions(User $user, float $percentLearnSkills): bool
    {
        $groupsCollection = new ArrayCollection();

        foreach ($user->getSkills() as $skill) {
            $groups = $this->groupManager->getGroupsBySkillId($skill->getId());

            foreach ($groups as $group) {
                if(!$groupsCollection->contains($group) && $group->getStudents()->count() < $group->getMaxStudentsCount()) {
                    $groupsCollection->add($group);
                }
            }
        }

        foreach ($groupsCollection as $group) {
            $groupSuggestion = $this->makeGroupSuggestion($user, $group, $percentLearnSkills);
            if($groupSuggestion !== null) {
                $this->groupSuggestionManager->saveGroupSuggestionFromEntity($groupSuggestion);
            }
        }

        return true;
    }
}