<?php

namespace App\Manager;

use App\DTO\SaveLessonDTO;
use App\DTO\UpdateLessonDTO;
use App\Entity\Group;
use App\Entity\Lesson;
use App\Entity\Skill;
use App\Entity\User;
use App\Repository\LessonRepository;
use Doctrine\ORM\EntityManagerInterface;

class LessonManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getLessons(int $page, int $perPage): array
    {
        /** @var LessonRepository $lessonRepository */
        $lessonRepository = $this->entityManager->getRepository(Lesson::class);

        return $lessonRepository->getLessons($page, $perPage);
    }

    public function getLessonById(int $lessonId): ?Lesson
    {
        /** @var LessonRepository $lessonRepository */
        $lessonRepository = $this->entityManager->getRepository(Lesson::class);

        return $lessonRepository->find($lessonId);
    }

    public function saveLessonFromDTO(Lesson $lesson, SaveLessonDTO $saveLessonDTO): ?int
    {
        $lesson->setDate($saveLessonDTO->date);
        $lesson->setDurationInMinutes($saveLessonDTO->durationInMinutes);
        if($saveLessonDTO->teacher !== null) {
            $lesson->setTeacher($this->entityManager->getRepository(User::class)->find($saveLessonDTO->teacher));
        }
        $lesson->setGroup($this->entityManager->getRepository(Group::class)->find($saveLessonDTO->group));
        $lesson->setSkill($this->entityManager->getRepository(Skill::class)->find($saveLessonDTO->skill));

        $this->entityManager->persist($lesson);
        $this->entityManager->flush();

        return $lesson->getId();
    }

    public function updateLessonFromDTO(int $lessonId, UpdateLessonDTO $updateLessonDTO): ?Lesson
    {
        /** @var LessonRepository $lessonRepository */
        $lessonRepository = $this->entityManager->getRepository(Lesson::class);
        /** @var Lesson $lesson */
        $lesson = $lessonRepository->find($lessonId);

        if ($lesson === null) {
            return null;
        }

        $lesson->setDate($updateLessonDTO->date);
        $lesson->setDurationInMinutes($updateLessonDTO->durationInMinutes);

        $this->entityManager->persist($lesson);
        $this->entityManager->flush();

        return $lesson;
    }

    public function addTeacher(int $lessonId, int $teacherId): bool
    {
        $teacher = $this->entityManager->getRepository(User::class)->findOneBy(['id'=> $teacherId, 'isTeacher' => true]);
        $lesson = $this->entityManager->getRepository(Lesson::class)->find($lessonId);

        if ($teacher !== null && $lesson !== null) {
            $lesson->setTeacher($teacher);

            $this->entityManager->persist($lesson);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function deleteLesson(int $lessonId): bool
    {
        /** @var LessonRepository $lessonRepository */
        $lessonRepository = $this->entityManager->getRepository(Lesson::class);
        /** @var Lesson $lesson */
        $lesson = $lessonRepository->find($lessonId);

        if ($lesson !== null) {
            $this->entityManager->remove($lesson);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }
}