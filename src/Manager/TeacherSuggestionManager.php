<?php

namespace App\Manager;

use App\Entity\GroupSuggestion;
use App\Entity\TeacherSuggestion;
use App\Repository\GroupSuggestionRepository;
use App\Repository\TeacherSuggestionRepository;
use Doctrine\ORM\EntityManagerInterface;

class TeacherSuggestionManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getTeacherSuggestions(int $page, int $perPage): array
    {
        /** @var TeacherSuggestionRepository $teacherSuggestionRepository */
        $teacherSuggestionRepository = $this->entityManager->getRepository(TeacherSuggestion::class);

        return $teacherSuggestionRepository->getTeacherSuggestions($page, $perPage);
    }

    public function getTeacherSuggestionById(int $teacherSuggestionId): ?TeacherSuggestion
    {
        /** @var TeacherSuggestionRepository $teacherSuggestionRepository */
        $teacherSuggestionRepository = $this->entityManager->getRepository(TeacherSuggestion::class);

        return $teacherSuggestionRepository->find($teacherSuggestionId);
    }

    public function getTeacherSuggestionByGroupId(int $groupId): array
    {
        /** @var TeacherSuggestionRepository $teacherSuggestionRepository */
        $teacherSuggestionRepository = $this->entityManager->getRepository(TeacherSuggestion::class);

        return $teacherSuggestionRepository->getTeacherSuggestionByGroupId($groupId);
    }

    public function deleteTeacherSuggestion(int $teacherSuggestionId): bool
    {
        /** @var TeacherSuggestionRepository $teacherSuggestionRepository */
        $teacherSuggestionRepository = $this->entityManager->getRepository(TeacherSuggestion::class);

        /** @var GroupSuggestion $groupSuggestion */
        $teacherSuggestion = $teacherSuggestionRepository->find($teacherSuggestionId);

        if ($teacherSuggestion !== null) {
            $this->entityManager->remove($teacherSuggestion);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}