<?php

namespace App\Manager;

use App\Entity\GroupSuggestion;
use App\Repository\GroupSuggestionRepository;
use Doctrine\ORM\EntityManagerInterface;

class GroupSuggestionManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getGroupSuggestions(int $page, int $perPage): array
    {
        /** @var GroupSuggestionRepository $groupSuggestionRepository */
        $groupSuggestionRepository = $this->entityManager->getRepository(GroupSuggestion::class);

        return $groupSuggestionRepository->getGroupSuggestions($page, $perPage);
    }

    public function getGroupSuggestionById(int $groupSuggestionId): ?GroupSuggestion
    {
        /** @var GroupSuggestionRepository $groupSuggestionRepository */
        $groupSuggestionRepository = $this->entityManager->getRepository(GroupSuggestion::class);

        return $groupSuggestionRepository->find($groupSuggestionId);
    }

    public function getGroupSuggestionByStudentId(int $studentId): array
    {
        /** @var GroupSuggestionRepository $groupSuggestionRepository */
        $groupSuggestionRepository = $this->entityManager->getRepository(GroupSuggestion::class);

        return $groupSuggestionRepository->getGroupSuggestionByStudentId($studentId);
    }

    public function deleteGroupSuggestion(int $groupId): bool
    {
        /** @var GroupSuggestionRepository $groupSuggestionRepository */
        $groupSuggestionRepository = $this->entityManager->getRepository(GroupSuggestion::class);

        /** @var GroupSuggestion $groupSuggestion */
        $groupSuggestion = $groupSuggestionRepository->find($groupId);

        if ($groupSuggestion !== null) {
            $this->entityManager->remove($groupSuggestion);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function saveGroupSuggestionFromEntity(GroupSuggestion $groupSuggestion): int
    {
        $this->entityManager->persist($groupSuggestion);
        $this->entityManager->flush();

        return $groupSuggestion->getId();
    }
}