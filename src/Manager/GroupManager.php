<?php

namespace App\Manager;

use App\DTO\SaveGroupDTO;
use App\DTO\SaveGroupFormDTO;
use App\DTO\UpdateGroupDTO;
use App\Entity\Group;
use App\Entity\Lesson;
use App\Entity\Skill;
use App\Entity\User;
use App\Repository\GroupRepository;
use App\Repository\LessonRepository;
use App\Repository\SkillRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class GroupManager
{
    private EntityManagerInterface $entityManager;

    private FormFactoryInterface $formFactory;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
    }

    public function getGroups(int $page, int $perPage): array
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        return $groupRepository->getGroups($page, $perPage);
    }

    public function getGroupById(int $groupId): ?Group
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        return $groupRepository->find($groupId);
    }

    public function saveGroupFromDTO(Group $group, SaveGroupDTO $saveGroupDTO): ?int
    {
        $group->setCode($saveGroupDTO->code);
        $group->setName($saveGroupDTO->name);
        $group->setMinStudentsCount($saveGroupDTO->minStudentsCount);
        $group->setMaxStudentsCount($saveGroupDTO->maxStudentsCount);

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return $group->getId();
    }

    public function updateGroupFromDTO(int $groupId, UpdateGroupDTO $updateGroupDTO): ?Group
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        /** @var Group $group */
        $group = $groupRepository->find($groupId);

        if ($group === null) {
            return null;
        }

        $group->setName($updateGroupDTO->name);
        $group->setMinStudentsCount($updateGroupDTO->minStudentsCount);
        $group->setMaxStudentsCount($updateGroupDTO->maxStudentsCount);

        $this->entityManager->flush();

        return $group;
    }

    public function addSkill(int $groupId, int $skillId): bool
    {
        $skill = $this->entityManager->getRepository(Skill::class)->find($skillId);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($skill !== null && $group !== null) {
            $group->addSkill($skill);
            $skill->addGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function removeSkill(int $groupId, int $skillId): bool
    {
        $skill = $this->entityManager->getRepository(Skill::class)->find($skillId);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($skill !== null && $group !== null) {
            $group->removeSkill($skill);
            $skill->removeGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function addLesson(int $groupId, int $lessonId): bool
    {
        $lesson = $this->entityManager->getRepository(Lesson::class)->find($lessonId);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($lesson !== null && $group !== null) {
            $group->addLesson($lesson);
            $lesson->setGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function removeLesson(int $groupId, int $lessonId): bool
    {
        $lesson = $this->entityManager->getRepository(Lesson::class)->find($lessonId);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($lesson !== null && $group !== null) {
            $group->removeLesson($lesson);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function addTeacher(int $groupId, int $teacherId): bool
    {
        $teacher = $this->entityManager->getRepository(User::class)->findOneBy(['id'=> $teacherId, 'isTeacher' => true]);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($teacher !== null && $group !== null) {
            $group->addTeacher($teacher);
            $teacher->addTeachersGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function removeTeacher(int $groupId, int $teacherId): bool
    {
        $teacher = $this->entityManager->getRepository(User::class)->findOneBy(['id'=> $teacherId, 'isTeacher' => true]);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($teacher !== null && $group !== null) {
            $group->removeTeacher($teacher);
            $teacher->removeTeachersGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function addStudent(int $groupId, int $studentId): bool
    {
        $student = $this->entityManager->getRepository(User::class)->findOneBy(['id'=> $studentId, 'isStudent' => true]);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($student !== null && $group !== null) {
            $group->addStudent($student);
            $student->addStudentsGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function removeStudent(int $groupId, int $studentId): bool
    {
        $student = $this->entityManager->getRepository(User::class)->findOneBy(['id'=> $studentId, 'isStudent' => true]);
        $group = $this->entityManager->getRepository(Group::class)->find($groupId);

        if ($student !== null && $group !== null) {
            $group->removeStudent($student);
            $student->removeStudentsGroup($group);

            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function deleteGroup(int $groupId): bool
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        /** @var Group $group */
        $group = $groupRepository->find($groupId);

        if ($group !== null) {
            $this->entityManager->remove($group);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    public function getSaveForm(): FormInterface
    {
        return $this->formFactory->createBuilder()
            ->add('code', TextType::class, ['required' => true])
            ->add('name', TextType::class, ['required' => true])
            ->add('minStudentsCount', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('maxStudentsCount', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('skills', EntityType::class, [
                'class' => Skill::class,
                'choice_label' => 'code',
                'multiple' => true
            ])
            ->add('submit', SubmitType::class)
            ->getForm();
    }

    public function saveGroupFormFromDTO(Group $group, SaveGroupFormDTO $saveGroupFormDTO): ?int
    {
        $group->setCode($saveGroupFormDTO->code);
        $group->setName($saveGroupFormDTO->name);
        $group->setMinStudentsCount($saveGroupFormDTO->minStudentsCount);
        $group->setMaxStudentsCount($saveGroupFormDTO->maxStudentsCount);
        foreach($saveGroupFormDTO->skills as $skill) {
            $group->addSkill($skill);
            $skill->addGroup($group);
            $this->entityManager->persist($skill);
        }

        foreach($saveGroupFormDTO->lessons as $lesson) {
            $group->addLesson($lesson);
            $lesson->setGroup($group);
            $this->entityManager->persist($lesson);
        }

        foreach($saveGroupFormDTO->students as $student) {
            $group->addStudent($student);
            $student->addStudentsGroup($group);
            $this->entityManager->persist($student);
        }

        foreach($saveGroupFormDTO->teachers as $teacher) {
            $group->addTeacher($teacher);
            $teacher->addTeachersGroup($group);
            $this->entityManager->persist($teacher);
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return $group->getId();
    }

    public function getUpdateForm(int $groupId): ?FormInterface
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);
        /** @var Group $group */
        $group = $groupRepository->find($groupId);
        if ($group === null) {
            return null;
        }

        return $this->formFactory->createBuilder(FormType::class, SaveGroupFormDTO::fromEntity($group))
            ->add('code', TextType::class, ['required' => true])
            ->add('name', TextType::class, ['required' => true])
            ->add('minStudentsCount', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('maxStudentsCount', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('skills', EntityType::class, [
                'class' => Skill::class,
                'query_builder' => function (SkillRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.code', 'ASC');
                },
                'choice_label' => 'code',
                'multiple' => true,
                'placeholder' => 'Choose skills',
            ])
            ->add('lessons', EntityType::class, [
                'class' => Lesson::class,
                'query_builder' => function (LessonRepository $er) use ($group) {
                    return $er->createQueryBuilder('l')
                        ->where('l.group = :group_id')
                        ->setParameter('group_id', $group->getId())
                        ->orWhere('l.group IS NULL')
                        ->orderBy('l.date', 'ASC');
                },
                'choice_label' => function ($lesson) {
                    return $lesson->getDate()->format('d-m-Y H:i:s'). ' ' . $lesson->getSkill()->getCode();
                },
                'multiple' => true,
                'required' => false
            ])
            ->add('students', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.isStudent = true')
                        ->orderBy('s.id', 'ASC');
                },
                'choice_label' => function ($user) {
                    return $user->getName(). ' ' . $user->getSurname();
                },
                'multiple' => true,
                'required' => false,
                'placeholder' => 'Choose students',
            ])
            ->add('teachers', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.isTeacher = true')
                        ->orderBy('t.id', 'ASC');
                },
                'choice_label' => function ($user) {
                    return $user->getName(). ' ' . $user->getSurname();
                },
                'multiple' => true,
                'required' => false,
                'placeholder' => 'Choose teachers',
            ])
            ->add('submit', SubmitType::class)
            ->setMethod('PATCH')
            ->getForm();
    }

    public function updateUserFormFromDTO(int $groupId, SaveGroupFormDTO $saveGroupFormDTO): bool
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);
        /** @var Group $group */
        $group = $groupRepository->find($groupId);
        if ($group === null) {
            return false;
        }

        $group->resetSkills();
        $group->resetLessons();
        $group->resetStudents();
        $group->resetTeachers();

        return $this->saveGroupFormFromDTO($group, $saveGroupFormDTO);
    }

    public function getGroupsBySkillId(int $skillId): array
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        return $groupRepository->getBySkillID($skillId);
    }
}