<?php

namespace App\Manager;

use App\DTO\SaveSkillDTO;
use App\DTO\UpdateSkillDTO;
use App\Entity\Skill;
use App\Repository\SkillRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class SkillManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function saveSkillFromDTO(Skill $skill, SaveSkillDTO $saveSkillDTO): ?int
    {
        $skill->setCode($saveSkillDTO->code);
        $skill->setName($saveSkillDTO->name);
        $skill->setLevel($saveSkillDTO->level);
        $skill->setLessonsCount($saveSkillDTO->lessonsCount);

        $this->entityManager->persist($skill);
        $this->entityManager->flush();

        return $skill->getId();
    }

    public function updateSkillFromDTO(int $skillId, UpdateSkillDTO $updateSkillDTO): ?Skill
    {
        /** @var SkillRepository $skillRepository */
        $skillRepository = $this->entityManager->getRepository(Skill::class);
        /** @var Skill $skill */
        $skill = $skillRepository->find($skillId);

        if ($skill === null) {
            return null;
        }

        $skill->setName($updateSkillDTO->name);
        $skill->setLevel($updateSkillDTO->level);
        $skill->setLessonsCount($updateSkillDTO->lessonsCount);
        $this->entityManager->persist($skill);
        $this->entityManager->flush();

        return $skill;
    }

    public function getSkills(int $page, int $perPage): array
    {
        /** @var SkillRepository $skillRepository */
        $skillRepository = $this->entityManager->getRepository(Skill::class);

        return $skillRepository->getSkills($page, $perPage);
    }

    public function getSkillById(int $skillId): ?Skill
    {
        /** @var SkillRepository $skillRepository */
        $skillRepository = $this->entityManager->getRepository(Skill::class);

        return $skillRepository->find($skillId);
    }

    public function deleteSkill(int $skillId): bool
    {
        /** @var SkillRepository $skillRepository */
        $skillRepository = $this->entityManager->getRepository(Skill::class);
        /** @var Skill $skill */
        $skill = $skillRepository->find($skillId);

        if ($skill !== null) {
            $this->entityManager->remove($skill);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }
}