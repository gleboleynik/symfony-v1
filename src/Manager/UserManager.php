<?php

namespace App\Manager;

use App\DTO\SaveUserDTO;
use App\DTO\UpdateUserDTO;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
    private EntityManagerInterface $entityManager;

    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function saveUser(string $login, string $password, string $email, bool $isStudent = false, bool $isTeacher = false): ?int
    {
        $user = new User();
        $user->setLogin($login);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, $password));
        $user->setEmail($email);
        $user->setIsStudent($isStudent);
        $user->setIsTeacher($isTeacher);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user->getId();
    }

    public function saveUserFromDTO(User $user, SaveUserDTO $SaveUserDTO): ?int
    {
        $user->setLogin($SaveUserDTO->login);
        $user->setName($SaveUserDTO->name);
        $user->setSurname($SaveUserDTO->surname);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, $SaveUserDTO->password));
        $user->setEmail($SaveUserDTO->email);
        $user->setPhone($SaveUserDTO->phone);
        $user->setRoles($SaveUserDTO->roles);
        $user->setIsStudent($SaveUserDTO->isStudent);
        $user->setIsTeacher($SaveUserDTO->isTeacher);
        $user->setIsActive($SaveUserDTO->isActive);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user->getId();
    }

    public function updateUserFromDTO(int $userId, UpdateUserDTO $updateUserDTO): ?User
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->find($userId);

        if ($user === null) {
            return null;
        }

        $user->setName($updateUserDTO->name);
        $user->setSurname($updateUserDTO->surname);
        $user->setEmail($updateUserDTO->email);
        $user->setPhone($updateUserDTO->phone);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function deleteUser(int $userId): bool
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->find($userId);

        if ($user !== null) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }

    public function getUsersByType(int $page, int $perPage, string $type): array
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);

        $users = match ($type) {
            'students' => $userRepository->getStudents($page, $perPage),
            'teachers'=> $userRepository->getTeachers($page, $perPage),
            default => [],
        };

        return $users;
    }

    public function getUserById(int $userId): ?User
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);

        return $userRepository->find($userId);
    }

    public function findUserByLogin(string $login): ?User
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        /** @var User|null $user */
        $user = $userRepository->findOneBy(['login' => $login]);

        return $user;
    }
}