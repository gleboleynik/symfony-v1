<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $admin->setLogin("admin");
        $admin->setName("Administrator");
        $admin->setSurname("Of Company");
        $admin->setPassword($this->passwordHasher->hashPassword($admin, "12345"));
        $admin->setEmail("admin@local.local");
        $admin->setPhone("79990123456");
        $admin->setRoles(["ROLE_ADMIN"]);
        $admin->setIsActive(true);
        $manager->persist($admin);

        $managerUser = new User();
        $managerUser->setLogin("manager");
        $managerUser->setName("Manager");
        $managerUser->setSurname("Of Courses");
        $managerUser->setPassword($this->passwordHasher->hashPassword($managerUser, "12345"));
        $managerUser->setEmail("manager@local.local");
        $managerUser->setPhone("79990123456");
        $managerUser->setRoles(["ROLE_MANAGER"]);
        $managerUser->setIsActive(true);
        $manager->persist($managerUser);

        $manager->flush();
    }
}
