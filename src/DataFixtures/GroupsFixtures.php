<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Lesson;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class GroupsFixtures extends Fixture implements DependentFixtureInterface
{
    public const SKILLS = [
        SkillsFixtures::PHP1,
        SkillsFixtures::PHP2,
        SkillsFixtures::LARAVEL,
        SkillsFixtures::SYMFONY,
        SkillsFixtures::JS1,
        SkillsFixtures::JS2,
        SkillsFixtures::REACT,
        SkillsFixtures::VUE,
        SkillsFixtures::GIT,
        SkillsFixtures::DOCKER,
    ];

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create("ru_RU");

        for ($i = 0; $i <= 20; $i++) {
            $group = new Group();
            $group->setCode("Group_".$i);
            $group->setName("Group_".$i);
            $group->setMinStudentsCount($faker->numberBetween(5,8));
            $group->setMaxStudentsCount($faker->numberBetween(10,15));
            $manager->persist($group);

            for ($j = 0; $j < $faker->numberBetween(1, 3); $j++) {
                $skill = $this->getReference($faker->randomElement(self::SKILLS));
                $skill->addGroup($group);
                for($k = 1; $k <= $skill->getLessonsCount(); $k++) {
                    $lesson = new Lesson();
                    $lesson->setDate($faker->dateTimeBetween('now', '+24 week'));
                    $lesson->setDurationInMinutes($faker->randomElement([60, 90]));
                    $lesson->setSkill($skill);

                    $manager->persist($lesson);
                    $manager->flush();

                    $lesson->setGroup($group);

                    unset($lesson);
                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            SkillsFixtures::class,
        ];
    }
}
