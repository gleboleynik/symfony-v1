<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SkillsFixtures extends Fixture
{
    public const PHP1 = 'PHP1';
    public const PHP2 = 'PHP2';
    public const LARAVEL = 'LARAVEL';
    public const SYMFONY = 'SYMFONY';
    public const JS1 = 'JS1';
    public const JS2 = 'JS2';
    public const REACT = 'REACT';
    public const VUE = 'VUE';
    public const GIT = 'GIT';
    public const DOCKER = 'DOCKER';

    public function load(ObjectManager $manager): void
    {
        $skill1 = new Skill();
        $skill1->setCode("PHP1");
        $skill1->setName("PHP start");
        $skill1->setLevel(1);
        $skill1->setLessonsCount(5);

        $skill2 = new Skill();
        $skill2->setCode("PHP2");
        $skill2->setName("PHP for middle");
        $skill2->setLevel(2);
        $skill2->setLessonsCount(15);

        $skill3 = new Skill();
        $skill3->setCode("LARAVEL");
        $skill3->setName("Laravel Framework");
        $skill3->setLevel(1);
        $skill3->setLessonsCount(25);

        $skill4 = new Skill();
        $skill4->setCode("SYMFONY");
        $skill4->setName("Symfony");
        $skill4->setLevel(1);
        $skill4->setLessonsCount(25);

        $skill5 = new Skill();
        $skill5->setCode("JS1");
        $skill5->setName("Javascript start");
        $skill5->setLevel(1);
        $skill5->setLessonsCount(7);

        $skill6 = new Skill();
        $skill6->setCode("JS2");
        $skill6->setName("Javascript for profi");
        $skill6->setLevel(2);
        $skill6->setLessonsCount(14);

        $skill7 = new Skill();
        $skill7->setCode("REACT");
        $skill7->setName("React for JS middle developer");
        $skill7->setLevel(3);
        $skill7->setLessonsCount(22);

        $skill8 = new Skill();
        $skill8->setCode("VUE");
        $skill8->setName("VueJS for JS middle developer");
        $skill8->setLevel(3);
        $skill8->setLessonsCount(18);

        $skill9 = new Skill();
        $skill9->setCode("Git");
        $skill9->setName("Git for developer");
        $skill9->setLevel(1);
        $skill9->setLessonsCount(8);

        $skill10 = new Skill();
        $skill10->setCode("Docker");
        $skill10->setName("Docker for developer");
        $skill10->setLevel(1);
        $skill10->setLessonsCount(3);

        $manager->persist($skill1);
        $manager->persist($skill2);
        $manager->persist($skill3);
        $manager->persist($skill4);
        $manager->persist($skill5);
        $manager->persist($skill6);
        $manager->persist($skill7);
        $manager->persist($skill8);
        $manager->persist($skill9);
        $manager->persist($skill10);

        $manager->flush();

        $this->addReference(self::PHP1, $skill1);
        $this->addReference(self::PHP2, $skill2);
        $this->addReference(self::LARAVEL, $skill3);
        $this->addReference(self::SYMFONY, $skill4);
        $this->addReference(self::JS1, $skill5);
        $this->addReference(self::JS2, $skill6);
        $this->addReference(self::REACT, $skill7);
        $this->addReference(self::VUE, $skill8);
        $this->addReference(self::GIT, $skill9);
        $this->addReference(self::DOCKER, $skill10);
    }
}
