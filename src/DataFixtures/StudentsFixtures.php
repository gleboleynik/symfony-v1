<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class StudentsFixtures extends Fixture implements DependentFixtureInterface
{
    public const SKILLS = [
        SkillsFixtures::PHP1,
        SkillsFixtures::PHP2,
        SkillsFixtures::LARAVEL,
        SkillsFixtures::SYMFONY,
        SkillsFixtures::JS1,
        SkillsFixtures::JS2,
        SkillsFixtures::REACT,
        SkillsFixtures::VUE,
        SkillsFixtures::GIT,
        SkillsFixtures::DOCKER,
    ];

    public array $users = [];

    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create("ru_RU");

        for ($i = 0; $i <= 50; $i++) {
            $user = new User();
            $user->setLogin($faker->unique()->userName);
            $user->setName($faker->firstName);
            $user->setSurname($faker->lastName);
            $user->setPassword($this->passwordHasher->hashPassword($user, '12345'));
            $user->setEmail($faker->email);
            $user->setPhone("79990123456");
            $user->setRoles([]);
            $user->setIsActive(true);
            $user->setIsStudent(true);

            for ($j = 0; $j < $faker->numberBetween(1, 4); $j++) {
                $user->addSkill($this->getReference($faker->randomElement(self::SKILLS)));
            }
            $manager->persist($user);

            $this->addReference("student_".$i, $user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            SkillsFixtures::class,
        ];
    }
}
