<?php

namespace App\Repository;

use App\Entity\Group;
use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository
{
    /**
     * @return Group[]
     */
    public function getGroups(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from($this->getClassName(), 'g')
            ->orderBy('g.id', "DESC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->enableResultCache(null, "groups_{$page}_{$perPage}")->getResult();
    }

    /**
     * @return Group[]
     */
    public function getByStudentID(int $studentID): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from($this->getClassName(), 'g')
            ->join("g.students", 's')
            ->where('s.id = :student_id')
            ->setParameter('student_id', $studentID)
            ->orderBy('g.id', "ASC");

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Group[]
     */
    public function getBySkillID(int $skillID): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from($this->getClassName(), 'g')
            ->join("g.skills", 's')
            ->where('s.id = :skill_id')
            ->setParameter('skill_id', $skillID)
            ->orderBy('g.id', "ASC");

        return $qb->getQuery()->enableResultCache(null, "groups_by_skill_{$skillID}")->getResult();
    }

    /**
     * @return Group[]
     */
    public function getBySkillCode(string $skillCode): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from($this->getClassName(), 'g')
            ->join("g.skills", 's')
            ->where('s.code = :skill_code')
            ->setParameter('skill_code', $skillCode)
            ->orderBy('g.id', "ASC");

        return $qb->getQuery()->getResult();
    }
}