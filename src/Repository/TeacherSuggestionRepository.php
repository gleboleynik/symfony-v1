<?php

namespace App\Repository;

use App\Entity\TeacherSuggestion;
use Doctrine\ORM\EntityRepository;

class TeacherSuggestionRepository extends EntityRepository
{

    /**
     * @return TeacherSuggestion[]
     */
    public function getTeacherSuggestionByGroupID(int $groupID): array
    {
        return $this->findBy(['group' => $groupID]);
    }

    /**
     * @return TeacherSuggestion[]
     */
    public function getTeacherSuggestions(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from($this->getClassName(), 't')
            ->orderBy('t.id', "ASC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->getResult();
    }
}