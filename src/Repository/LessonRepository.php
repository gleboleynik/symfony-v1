<?php

namespace App\Repository;

use App\Entity\Lesson;
use Doctrine\ORM\EntityRepository;

class LessonRepository extends EntityRepository
{
    /**
     * @return Lesson[]
     */
    public function getLessonByGroupID(int $groupID): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('l')
            ->from($this->getClassName(), 'l')
            ->join('l.group', 'g')
            ->orderBy('l.date', "ASC")
            ->where('g.id = :group_id')
            ->setParameter('group_id', $groupID);

        return $qb->getQuery()->enableResultCache(null, "lessons_by_group_{$groupID}")->getResult();
    }

    /**
     * @return Lesson[]
     */
    public function getLessonByTeacher(int $teacherID): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('l')
            ->from($this->getClassName(), 'l')
            ->join('l.teacher', 't')
            ->orderBy('l.date', "ASC")
            ->where('t.id = :teacher_id')
            ->andWhere('t.isTeacher', 'true')
            ->setParameter('teacher_id', $teacherID);

        return $qb->getQuery()->enableResultCache(null, "lessons_by_teacher_{$teacherID}")->getResult();
    }

    /**
     * @return Lesson[]
     */
    public function getLessons(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('l')
            ->from($this->getClassName(), 'l')
            ->orderBy('l.id', "ASC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->enableResultCache(null, "lessons_{$page}_{$perPage}")->getResult();
    }
}