<?php

namespace App\Repository;

use App\Entity\GroupSuggestion;
use Doctrine\ORM\EntityRepository;

class GroupSuggestionRepository extends EntityRepository
{

    /**
     * @return GroupSuggestion[]
     */
    public function getGroupSuggestionByStudentId (int $studentId): array
    {
        return $this->findBy(['student' => $studentId]);
    }

    /**
     * @return GroupSuggestion[]
     */
    public function getGroupSuggestions(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from($this->getClassName(), 's')
            ->orderBy('s.id', "ASC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->getResult();
    }
}