<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]
     */
    public function getStudents(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from($this->getClassName(), 's')
            ->where('s.isStudent = true')
            ->orderBy('s.id', "ASC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->enableResultCache(null, "students_{$page}_{$perPage}")->getResult();
    }

    /**
     * @return User[]
     */
    public function getTeachers(int $page, int $perPage): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from($this->getClassName(), 's')
            ->where('s.isTeachers = true')
            ->orderBy('s.id', "ASC")
            ->setFirstResult($perPage * $page)
            ->setMaxResults($perPage);

        return $qb->getQuery()->enableResultCache(null, "teachers_{$page}_{$perPage}")->getResult();
    }

    public function getUserByLogin(string $login): ?User
    {
        return $this->findOneBy(['login' => $login]);
    }
}
