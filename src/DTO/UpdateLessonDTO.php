<?php

namespace App\DTO;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateLessonDTO
{
    #[Assert\NotBlank]
    #[Assert\Type('DateTime')]
    public DateTime $date;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $durationInMinutes;
}