<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class SaveSkillDTO
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 20)]
    #[Assert\Type('string')]
    public string $code;

    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $level;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $lessonsCount;
}