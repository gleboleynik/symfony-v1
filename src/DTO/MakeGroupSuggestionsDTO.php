<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class MakeGroupSuggestionsDTO
{
    #[Assert\NotBlank()]
    #[Assert\Type('integer')]
    public int $userId;

    #[Assert\Type('float')]
    public float $percentLearnedSkills = 50.0;
}