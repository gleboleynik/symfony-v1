<?php

namespace App\DTO;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class SaveLessonDTO
{
    #[Assert\NotBlank]
    #[Assert\Type('DateTime')]
    public DateTime $date;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $durationInMinutes;

    #[Assert\Type('int')]
    public ?int $teacher;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $group;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $skill;
}