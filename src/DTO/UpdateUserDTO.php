<?php

namespace App\DTO;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateUserDTO
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 32)]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Length(max: 60)]
    #[Assert\Type('string')]
    public string $surname;

    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    #[Assert\Type('string')]
    public string $email;

    #[Assert\NotBlank]
    #[Assert\Length(max: 11)]
    #[Assert\Type('string')]
    public string $phone;
}