<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class SaveUserDTO
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 32)]
    #[Assert\Type('string')]
    public string $login;

    #[Assert\NotBlank]
    #[Assert\Length(max: 32)]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Length(max: 60)]
    #[Assert\Type('string')]
    public string $surname;

    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    #[Assert\Type('string')]
    public string $password;

    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    #[Assert\Type('string')]
    public string $email;

    #[Assert\NotBlank]
    #[Assert\Length(max: 11)]
    #[Assert\Type('string')]
    public string $phone;

    /** @var string[] */
    #[Assert\Type('array')]
    public array $roles;

    #[Assert\Type('boolean')]
    public bool $isStudent;

    #[Assert\Type('boolean')]
    public bool $isTeacher;

    #[Assert\Type('boolean')]
    public bool $isActive;
}