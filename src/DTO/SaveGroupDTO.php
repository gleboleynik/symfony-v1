<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class SaveGroupDTO
{
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $code;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $minStudentsCount = 0;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $maxStudentsCount;
}