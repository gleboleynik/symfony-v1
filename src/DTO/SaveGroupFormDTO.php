<?php

namespace App\DTO;

use App\Entity\Group;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class SaveGroupFormDTO
{
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $code;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    public string $name;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $minStudentsCount = 0;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    public int $maxStudentsCount;

    #[Assert\Type(Collection::class)]
    public Collection $skills;

    #[Assert\Type(Collection::class)]
    public Collection $lessons;

    #[Assert\Type(Collection::class)]
    public Collection $students;

    #[Assert\Type(Collection::class)]
    public Collection $teachers;

    public function __construct($data)
    {
        $this->code = $data['code'] ?? '';
        $this->name = $data['name'] ?? '';
        $this->minStudentsCount = (int)$data['minStudentsCount'] ?? 0;
        $this->maxStudentsCount = (int)$data['maxStudentsCount'] ?? 0;
        $this->skills = $data['skills'] ?? new ArrayCollection();
        $this->lessons = $data['lessons'] ?? new ArrayCollection();
        $this->students = $data['students'] ?? new ArrayCollection();
        $this->teachers = $data['teachers'] ?? new ArrayCollection();
    }

    public static function fromEntity(Group $group): self
    {
        return new self([
            'code' => $group->getCode(),
            'name' => $group->getName(),
            'minStudentsCount' => $group->getMinStudentsCount(),
            'maxStudentsCount' => $group->getMaxStudentsCount(),
            'skills' => $group->getSkills(),
            'lessons' => $group->getLessons(),
            'students' => $group->getStudents(),
            'teachers' => $group->getTeachers(),
        ]);
    }
}