<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Table(name: '`skill`')]
#[ORM\Entity(repositoryClass: SkillRepository::class)]
#[ORM\Index(columns: ['code'], name: 'lesson_code__idx')]
class Skill
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 20, unique: true, nullable: false)]
    private string $code;

    #[ORM\Column(type: 'string', length: 100, unique: true, nullable: false)]
    private string $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $level;

    #[ORM\Column(type: 'integer', nullable: false)]
    private int $lessonsCount;

    #[ORM\ManyToMany(targetEntity: 'Group', inversedBy: 'skills')]
    #[ORM\JoinTable(name: 'skills_groups')]
    #[ORM\JoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'group_id', referencedColumnName: 'id')]
    private Collection $groups;

    #[ORM\OneToMany(mappedBy: 'skill', targetEntity: 'Lesson', cascade: ['remove'])]
    private Collection $lessons;

    #[ORM\ManyToMany(targetEntity: 'User', mappedBy: 'skills', orphanRemoval: true)]
    #[ORM\JoinTable(name: 'skills_users')]
    #[ORM\JoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private Collection $users;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    public function getLessonsCount(): int
    {
        return $this->lessonsCount;
    }

    public function setLessonsCount(int $lessonsCount): void
    {
        $this->lessonsCount = $lessonsCount;
    }

    public function addGroup(Group $group): void
    {
        if (!$this->groups->contains($group)) {
            $this->groups->add($group);
        }
    }

    public function removeGroup(Group $group): void
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }
    }

    public function addLesson(Lesson $lesson): void
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons->add($lesson);
        }
    }

    public function removeLesson(Lesson $lesson): void
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    public function removeUser(User $user): void
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = new DateTime();
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = new DateTime();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'level' => $this->level,
            'lessons_count' => $this->lessonsCount,
            'students_groups' => array_map(
                static fn(Group $group) => [
                    'group_id' => $group->getId(),
                    'group_name' => $group->getName(),
                    'group_code' => $group->getCode(),
                ],
                $this->groups->toArray()
            ),
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];
    }
}