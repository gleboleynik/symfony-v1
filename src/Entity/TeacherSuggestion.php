<?php

namespace App\Entity;

use App\Repository\TeacherSuggestionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Table(name: '`teacher_suggestion`')]
#[ORM\Entity(repositoryClass: TeacherSuggestionRepository::class)]
class TeacherSuggestion
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\OneToOne(targetEntity: 'Group')]
    private Group $group;

    #[ORM\ManyToMany(targetEntity: 'Lesson')]
    #[ORM\JoinTable(name: 'teacher_suggestion__lessons')]
    #[ORM\JoinColumn(name: 'teacher_suggestion__id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'lesson_id', referencedColumnName: 'id', unique: true)]
    private Collection $lessons;

    #[ORM\OneToOne(targetEntity: 'User')]
    private User $teacher;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function __construct()
    {
        $this->lessons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): void
    {
        $this->group = $group;
    }

    public function addLesson(Lesson $lesson): void
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons->add($lesson);
        }
    }

    public function removeLesson(Lesson $lesson): void
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }
    }

    public function getTeacher(): User
    {
        return $this->teacher;
    }

    public function setTeacher(User $teacher): void
    {
        $this->teacher = $teacher;
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = new DateTime();
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = new DateTime();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'group' => [
                'group_id' => $this->group->getId(),
                'group_code' => $this->group->getCode(),
                'group_name' => $this->group->getName(),
            ],
            'lessons' => array_map(
                static fn(Lesson $lesson) => [
                    'lesson_id' => $lesson->getId(),
                    'date' => $lesson->getDate()->format('Y-m-d H:i:s'),
                    'teacher' => [
                        "teacher_id" => $lesson->getTeacher()->getId(),
                        "teacher_fullname" => $lesson->getTeacher()->getName(). ' ' .$lesson->getTeacher()->getSurname(),
                    ],
                    'skill' => [
                        'skill_id' => $lesson->getSkill()->getID(),
                        'skill_code' => $lesson->getSkill()->getCode(),
                        'skill_name' => $lesson->getSkill()->getName(),
                    ]
                ],
                $this->lessons->toArray()
            ),
            'teacher' => [
                'teacher_id' => $this->teacher->getId(),
                'teacher_fullname' => $this->teacher->getName(). ' ' .$this->teacher->getSurname(),
            ],
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];
    }
}