<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Table(name: '`user`')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Index(columns: ['login'], name: 'user__login__idx')]
#[ORM\Index(columns: ['is_student'], name: 'user__is_student__idx')]
#[ORM\Index(columns: ['is_teacher'], name: 'user__is_teacher__idx')]
#[ORM\UniqueConstraint(columns: ['login'], name: 'user__login__unique_idx')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 32, unique: true, nullable: false)]
    private string $login;

    #[ORM\Column(type: 'string', length: 32)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 60)]
    private ?string $surname = null;

    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    private string $password;

    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    private string $email;

    #[ORM\Column(type: 'string', length: 11)]
    private ?string $phone = null;

    #[ORM\Column(type: 'string', length: 1024, nullable: false)]
    private string $roles = '{}';

    #[ORM\Column(type: 'boolean', nullable: false, options: ['defaults' => false])]
    private bool $isActive = false;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['defaults' => false])]
    private bool $isStudent = false;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['defaults' => false])]
    private bool $isTeacher = false;

    #[ORM\ManyToMany(targetEntity: 'Group', inversedBy: 'students')]
    #[ORM\JoinTable(name: 'groups_students')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'group_id', referencedColumnName: 'id')]
    private Collection $studentsGroups;

    #[ORM\ManyToMany(targetEntity: 'Group', inversedBy: 'teachers')]
    #[ORM\JoinTable(name: 'groups_teachers')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'group_id', referencedColumnName: 'id')]
    private Collection $teachersGroups;

    #[ORM\ManyToMany(targetEntity: 'Skill', inversedBy: 'users')]
    #[ORM\JoinTable(name: 'skills_users')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Collection $skills;

    #[ORM\OneToMany(mappedBy: 'teacher', targetEntity: 'Lesson')]
    private Collection $teachersLessons;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function __construct()
    {
        $this->studentsGroups = new ArrayCollection();
        $this->teachersGroups = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->teachersLessons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string[]
     *
     * @throws JsonException
     */
    public function getRoles(): array
    {
        $roles = json_decode($this->roles, true, 512, JSON_THROW_ON_ERROR);
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param string[] $roles
     *
     * @throws JsonException
     */
    public function setRoles(array $roles): void
    {
        $this->roles = json_encode($roles, JSON_THROW_ON_ERROR);
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function isStudent(): bool
    {
        return $this->isStudent;
    }

    public function setIsStudent(bool $isStudent): void
    {
        $this->isStudent = $isStudent;
    }

    public function isTeacher(): bool
    {
        return $this->isTeacher;
    }

    public function setIsTeacher(bool $isTeacher): void
    {
        $this->isTeacher = $isTeacher;
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = new DateTime();
    }

    public function addStudentsGroup(Group $group): void
    {
        if (!$this->studentsGroups->contains($group)) {
            $this->studentsGroups->add($group);
        }
    }

    public function removeStudentsGroup(Group $group): void
    {
        if ($this->studentsGroups->contains($group)) {
            $this->studentsGroups->removeElement($group);
        }
    }

    public function getStudentsGroups(): Collection
    {
        return $this->studentsGroups;
    }

    public function addTeachersGroup(Group $group): void
    {
        if (!$this->teachersGroups->contains($group)) {
            $this->teachersGroups->add($group);
        }
    }

    public function removeTeachersGroup(Group $group): void
    {
        if ($this->teachersGroups->contains($group)) {
            $this->teachersGroups->removeElement($group);
        }
    }

    public function getTeachersGroups(): Collection
    {
        return $this->teachersGroups;
    }

    public function addSkill(Skill $skill): void
    {
        if (!$this->skills->contains($skill)) {
            $this->skills->add($skill);
        }
    }

    public function removeSkill(Skill $skill): void
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
        }
    }

    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addTeachersLesson(Lesson $lesson): void
    {
        if (!$this->teachersLessons->contains($lesson)) {
            $this->teachersLessons->add($lesson);
        }
    }

    public function removeTeacherLesson(Lesson $lesson): void
    {
        if ($this->teachersLessons->contains($lesson)) {
            $this->teachersLessons->removeElement($lesson);
        }
    }

    public function getTeachersLessons(): Collection
    {
        return $this->teachersLessons;
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = new DateTime();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'phone' => $this->phone,
            'roles' => $this->getRoles(),
            'is_active' => $this->isActive,
            'is_student' => $this->isStudent,
            'is_teacher' => $this->isTeacher,
            'students_groups' => array_map(
                fn(Group $group) => [
                    'group_id' => $group->getId(),
                    'group_name' => $group->getName(),
                    'group_code' => $group->getCode(),
                    'group_skills' => array_map(
                        static fn(Skill $skill) => [
                            'skill_id' => $skill->getId(),
                            'skill_code' => $skill->getCode(),
                            'skill_name' => $skill->getName(),
                            'skill_level' => $skill->getLevel()
                        ],
                        $group->getSkills()->toArray()
                    ),
                    'group_lessons' => array_map(
                        static fn(Lesson $lesson) => [
                            'lesson_id' => $lesson->getId(),
                            'lesson_date' => $lesson->getDate()->format('Y-m-d H:i:s'),
                            'lesson_duration_in_minutes' => $lesson->getDurationInMinutes()
                        ],
                        $group->getLessons()->toArray()
                    )
                ],
                $this->studentsGroups->toArray()
            ),
            'teachers_groups' => array_map(
                fn(Group $group) => [
                    'group_id' => $group->getId(),
                    'group_name' => $group->getName(),
                    'group_code' => $group->getCode(),
                    'group_skills' => array_map(
                        static fn(Skill $skill) => [
                            'skill_id' => $skill->getId(),
                            'skill_code' => $skill->getCode(),
                            'skill_name' => $skill->getName(),
                            'skill_level' => $skill->getLevel()
                        ],
                        $this->skills->toArray()
                    ),
                    'group_lessons' => array_map(
                        static fn(Lesson $lesson) => [
                            'lesson_id' => $lesson->getId(),
                            'lesson_date' => $lesson->getDate()->format('Y-m-d H:i:s'),
                            'lesson_duration_in_minutes' => $lesson->getDurationInMinutes()
                        ],
                        $group->getLessons()->toArray()
                    )
                ],
                $this->studentsGroups->toArray()
            ),
            'skills' => array_map(
                static fn(Skill $skill) => [
                    'skill_id' => $skill->getId(),
                    'skill_code' => $skill->getCode(),
                    'skill_name' => $skill->getName(),
                    'skill_level' => $skill->getLevel()
                ],
                $this->skills->toArray()
            ),
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function getUsername(): string
    {
        return $this->login;
    }
}