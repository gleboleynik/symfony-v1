<?php

namespace App\Entity;

use App\Repository\GroupSuggestionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Table(name: '`group_suggestion`')]
#[ORM\Entity(repositoryClass: GroupSuggestionRepository::class)]
class GroupSuggestion
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: 'Group')]
    private Group $group;

    #[ORM\ManyToMany(targetEntity: 'Skill')]
    #[ORM\JoinTable(name: 'suggestion_learned_skills')]
    #[ORM\JoinColumn(name: 'group_suggestion__id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Collection $learnedSkills;

    #[ORM\ManyToMany(targetEntity: 'Skill')]
    #[ORM\JoinTable(name: 'suggestion_not_learned_skills')]
    #[ORM\JoinColumn(name: 'group_suggestion__id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Collection $notLearnedSkills;

    #[ORM\ManyToMany(targetEntity: 'Skill')]
    #[ORM\JoinTable(name: 'suggestion_additional_learned_skills')]
    #[ORM\JoinColumn(name: 'group_suggestion__id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Collection $additionalLearnedSkills;

    #[ORM\ManyToOne(targetEntity: 'User')]
    private User $student;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function __construct() {
        $this->learnedSkills = new ArrayCollection();
        $this->notLearnedSkills = new ArrayCollection();
        $this->additionalLearnedSkills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): void
    {
        $this->group = $group;
    }

    public function setLearnedSkills(Collection $learnedSkills): void
    {
        $this->learnedSkills = $learnedSkills;
    }

    public function setNotLearnedSkills(Collection $notLearnedSkills): void
    {
        $this->notLearnedSkills = $notLearnedSkills;
    }

    public function setAdditionalLearnedSkills(Collection $additionalLearnedSkills): void
    {
        $this->additionalLearnedSkills = $additionalLearnedSkills;
    }

    public function getStudent(): User
    {
        return $this->student;
    }
    function setStudent(User $student): void
    {
        $this->student = $student;
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = DateTime::createFromFormat('U', (string)time());
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = DateTime::createFromFormat('U', (string)time());
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'group' => [
                'group_id' => $this->group->getId(),
                'group_code' => $this->group->getCode(),
                'group_name' => $this->group->getName(),
            ],
            'learned_skills' => array_map(
                static fn(Skill $skill) => [
                    'skill_id' => $skill->getId(),
                    'skill_code' => $skill->getCode(),
                    'skill_name' => $skill->getName(),
                    'skill_level' => $skill->getLevel()
                ],
                $this->learnedSkills->toArray()
            ),
            'not_learned_skills' => array_map(
                static fn(Skill $skill) => [
                    'skill_id' => $skill->getId(),
                    'skill_code' => $skill->getCode(),
                    'skill_name' => $skill->getName(),
                    'skill_level' => $skill->getLevel()
                ],
                $this->notLearnedSkills->toArray()
            ),
            'additional_learned_skills' => array_map(
                static fn(Skill $skill) => [
                    'skill_id' => $skill->getId(),
                    'skill_code' => $skill->getCode(),
                    'skill_name' => $skill->getName(),
                    'skill_level' => $skill->getLevel()
                ],
                $this->additionalLearnedSkills->toArray()
            ),
            'student' => [
                'student_id' => $this->student->getId(),
                'student_fullname' => $this->student->getName(). ' ' .$this->student->getSurname(),
            ],
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];
    }
}