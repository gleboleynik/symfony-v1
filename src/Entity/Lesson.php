<?php

namespace App\Entity;

use App\Repository\LessonRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Table(name: '`lesson`')]
#[ORM\Entity(repositoryClass: LessonRepository::class)]
#[ORM\Index(columns: ['teacher_id'], name: 'lesson__teacher_id__idx')]
class Lesson
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    private DateTime $date;

    #[ORM\Column(name: 'duration', type: 'integer', nullable: false)]
    private int $durationInMinutes;

    #[ORM\ManyToOne(targetEntity: 'User', inversedBy: 'teachersLessons')]
    #[ORM\JoinColumn(name: 'teacher_id', referencedColumnName: 'id')]
    private ?User $teacher;

    #[ORM\ManyToOne(targetEntity: 'Group', inversedBy: 'lessons')]
    #[ORM\JoinColumn(name: 'group_id', referencedColumnName: 'id')]
    private Group $group;

    #[ORM\ManyToOne(targetEntity: 'Skill', inversedBy: 'lessons')]
    #[ORM\JoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Skill $skill;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getDurationInMinutes(): int
    {
        return $this->durationInMinutes;
    }

    public function setDurationInMinutes(int $durationInMinutes): void
    {
        $this->durationInMinutes = $durationInMinutes;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(User $teacher): void
    {
        $this->teacher = $teacher;
    }

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): void
    {
        $this->group = $group;
    }

    public function getSkill(): Skill
    {
        return $this->skill;
    }

    public function setSkill(Skill $skill): void
    {
        $this->skill = $skill;
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = new DateTime();
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = new DateTime();
    }

    public function toArray(): array
    {
        $result = [
            'id' => $this->id,
            'date' => $this->date->format('Y-m-d H:i:s'),
            'durationInMinutes' => $this->durationInMinutes,
            'teacher' => [],
            'group' => [
                'group_id' => $this->group->getId(),
                'group_code' => $this->group->getCode(),
                'group_name' => $this->group->getName(),
            ],
            'skills' => $this->skill->toArray(),
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];

        if(!empty($this->getTeacher())) {
            $result['teacher'] = [
                'teacher_id' => $this->teacher?->getId(),
                'teacher_login' => $this->teacher?->getLogin(),
                'teacher_name' => $this->teacher?->getName(),
                'teacher_surname' => $this->teacher?->getSurname(),
                'teacher_skills' => array_map(
                    static fn(Skill $skill) => [
                        'skill_id' => $skill->getId(),
                        'skill_code' => $skill->getCode(),
                        'skill_name' => $skill->getName(),
                        'skill_level' => $skill->getLevel()
                    ],
                    $this->teacher->getSkills()->toArray()
                ),
            ];
        }

        return $result;

    }
}