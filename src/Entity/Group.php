<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Table(name: '`group`')]
#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\UniqueConstraint(columns: ['code'], name: 'group__code__unique_idx')]
class Group
{
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 20, unique: true, nullable: false)]
    private string $code;

    #[ORM\Column(type: 'string', length: 100, unique: true, nullable: false)]
    private string $name;

    #[ORM\Column(type: 'integer', nullable: false)]
    private int $minStudentsCount = 0;

    #[ORM\Column(type: 'integer', nullable: false)]
    private int $maxStudentsCount;

    #[ORM\ManyToMany(targetEntity: 'Skill', mappedBy: 'groups')]
    #[ORM\JoinTable(name: 'skills_groups')]
    #[ORM\JoinColumn(name: 'group_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Collection $skills;

    #[ORM\OneToMany(mappedBy: 'group', targetEntity: 'Lesson')]
    private Collection $lessons;

    #[ORM\ManyToMany(targetEntity: 'User', mappedBy: 'studentsGroups')]
    #[ORM\JoinTable(name: 'groups_students')]
    #[ORM\JoinColumn(name: 'group_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private Collection $students;

    #[ORM\ManyToMany(targetEntity: 'User', mappedBy: 'teachersGroups')]
    #[ORM\JoinTable(name: 'groups_teachers')]
    #[ORM\JoinColumn(name: 'group_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private Collection $teachers;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private DateTime $updatedAt;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->teachers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMinStudentsCount(): int
    {
        return $this->minStudentsCount;
    }

    public function setMinStudentsCount(int $minStudentsCount): void
    {
        $this->minStudentsCount = $minStudentsCount;
    }

    public function getMaxStudentsCount(): int
    {
        return $this->maxStudentsCount;
    }

    public function setMaxStudentsCount(int $maxStudentsCount): void
    {
        $this->maxStudentsCount = $maxStudentsCount;
    }

    public function addSkill(Skill $skill): void
    {
        if (!$this->skills->contains($skill)) {
            $this->skills->add($skill);
        }
    }

    public function removeSkill(Skill $skill): void
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
        }
    }

    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function resetSkills(): Collection
    {
        return new ArrayCollection();
    }

    public function addLesson(Lesson $lesson): void
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons->add($lesson);
        }
    }

    public function removeLesson(Lesson $lesson): void
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }
    }

    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function resetLessons(): Collection
    {
        return new ArrayCollection();
    }

    public function addStudent(User $student): void
    {
        if (!$this->students->contains($student)) {
            $this->students->add($student);
        }
    }

    public function removeStudent(User $student): void
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
        }
    }

    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function resetStudents(): Collection
    {
        return new ArrayCollection();
    }

    public function addTeacher(User $teacher): void
    {
        if (!$this->teachers->contains($teacher)) {
            $this->teachers->add($teacher);
        }
    }

    public function removeTeacher(User $teacher): void
    {
        if ($this->teachers->contains($teacher)) {
            $this->teachers->removeElement($teacher);
        }
    }

    public function getTeachers(): Collection
    {
        return $this->teachers;
    }

    public function resetTeachers(): Collection
    {
        return new ArrayCollection();
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setCreatedAt(): void {
        $this->createdAt = new DateTime();
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void {
        $this->updatedAt = new DateTime();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'minStudentsCount' => $this->minStudentsCount,
            'maxStudentsCount' => $this->maxStudentsCount,
            'skills' => array_map(
                static fn(Skill $skill) => [
                    'skill_id' => $skill->getId(),
                    'skill_code' => $skill->getCode(),
                    'skill_name' => $skill->getName(),
                    'skill_level' => $skill->getLevel()
                ],
                $this->skills->toArray()
            ),
            'lessons' => array_map(
                static fn(Lesson $lesson) => [
                    'lesson_id' => $lesson->getId(),
                    'date' => $lesson->getDate()->format('Y-m-d H:i:s'),
                    'teacher' => [
                        "teacher_id" => $lesson->getTeacher()?->getId(),
                        "teacher_fullname" => $lesson->getTeacher()?->getName(). ' ' .$lesson->getTeacher()?->getSurname(),
                    ],
                    'skill' => [
                        'skill_id' => $lesson->getSkill()->getID(),
                        'skill_code' => $lesson->getSkill()->getCode(),
                        'skill_name' => $lesson->getSkill()->getName(),
                    ]
                ],
                $this->lessons->toArray()
            ),
            'students' => array_map(
                static fn(User $student) => [
                    'student_id' => $student?->getId(),
                    "student_fullname" => $student?->getName(). ' ' .$student?->getSurname(),
                ],
                $this->students->toArray()
            ),
            'teachers' => array_map(
                static fn(User $teacher) => [
                    'teacher_id' => $teacher?->getId(),
                    "teacher_fullname" => $teacher?->getName(). ' ' .$teacher?->getSurname(),
                ],
                $this->students->toArray()
            ),
            'created_at' => $this->createdAt->format('Y-m-d H:i:s'),
            'updated_at' => $this->updatedAt->format('Y-m-d H:i:s'),
        ];
    }
}