<?php

namespace App\Consumer\MakeGroupSuggestions;

use App\DTO\MakeGroupSuggestionsDTO;
use App\Entity\User;
use App\Service\GroupSuggestionsService;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Consumer implements ConsumerInterface
{
    private EntityManagerInterface $entityManager;

    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    private GroupSuggestionsService $groupSuggestionsService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        GroupSuggestionsService $groupSuggestionsService
    )
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->groupSuggestionsService = $groupSuggestionsService;
    }

    public function execute(AMQPMessage $msg): int
    {
        $message = $this->serializer->deserialize($msg->getBody(), MakeGroupSuggestionsDTO::class, 'json');
        $errors = $this->validator->validate($message);

        if ($errors->count() > 0) {
            return $this->reject((string)($errors));
        }

        $userRepository = $this->entityManager->getRepository(User::class);
        $user = $userRepository->find($message->userId);
        if (!($user instanceof User)) {
            return $this->reject(sprintf('User ID %s was not found', $message->userId()));
        }

        $this->groupSuggestionsService->makeGroupSuggestions($user, $message->percentLearnedSkills);

        $this->entityManager->clear();
        $this->entityManager->getConnection()->close();

        return self::MSG_ACK;
    }

    private function reject(string $error): int
    {
        echo "Incorrect message: $error";

        return self::MSG_REJECT;
    }
}