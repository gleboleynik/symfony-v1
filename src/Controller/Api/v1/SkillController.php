<?php

namespace App\Controller\Api\v1;

use App\DTO\SaveSkillDTO;
use App\DTO\UpdateSkillDTO;
use App\Entity\Skill;
use App\Manager\SkillManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: 'api/v1/skill')]
class SkillController extends AbstractController
{
    private SkillManager $skillManager;

    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    public function __construct(SkillManager $skillManager, ValidatorInterface $validator, SerializerInterface  $serializer)
    {
        $this->skillManager = $skillManager;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getSkill(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");

        $skills = $this->skillManager->getSkills($page, $perPage);

        [$data, $code] = empty($skills) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'skills' => array_map(
                static fn (Skill $skill) => ['skill' => $skill->toArray()],
                $skills)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getSkillById(int $id, Request $request): Response
    {
        $skill = $this->skillManager->getSkillById($id);

        [$data, $code] = empty($skill) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'skill' => $skill->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '', methods: ['POST'])]
    public function addSkill(Request $request): Response
    {
        $saveSkillDTO = $this->serializer->deserialize($request->getContent(), SaveSkillDTO::class, 'json');
        $errors = $this->validator->validate($saveSkillDTO);

        $skillId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $skillId = $this->skillManager->saveSkillFromDTO(new Skill(), $saveSkillDTO);
        }

        [$data, $code] = $skillId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'skillId' => $skillId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['POST'], requirements: ['id' => '\d+'])]
    public function updateSkill(Skill $skill, Request $request): Response
    {
        $updateSkillDTO = $this->serializer->deserialize($request->getContent(), UpdateSkillDTO::class, 'json');
        $errors = $this->validator->validate($updateSkillDTO);

        $skillId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $skill = $this->skillManager->updateSkillFromDTO($skill->getId(), $updateSkillDTO);
            $skillId = $skill->getId();
        }

        [$data, $code] = $skillId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'skillId' => $skillId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteSkill(int $id): Response
    {
        $success = $this->skillManager->deleteSkill($id);

        [$data, $code] = $success ?
            [['success' => true], Response::HTTP_OK] :
            [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }
}