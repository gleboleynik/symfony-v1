<?php

namespace App\Controller\Api\v1;

use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/group')]
class GroupTeacherController extends AbstractController
{
    private GroupManager $groupManager;

    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    #[Route(path: '/{groupId}/add-teacher/{teacherId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'teacherId' => '\d+'])]
    public function addTeacher(int $groupId, int $teacherId, Request $request): Response
    {
        $success = $this->groupManager->addTeacher($groupId, $teacherId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t add this user as teacher'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{groupId}/remove-teacher/{teacherId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'teacherId' => '\d+'])]
    public function removeTeacher(int $groupId, int $teacherId, Request $request): Response
    {
        $success = $this->groupManager->removeTeacher($groupId, $teacherId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t remove this user as teacher'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }
}