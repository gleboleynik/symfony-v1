<?php

namespace App\Controller\Api\v1;

use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/group')]
class GroupStudentController extends AbstractController
{
    private GroupManager $groupManager;

    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }
    #[Route(path: '/{groupId}/add-student/{studentId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'studentId' => '\d+'])]
    public function addStudent(int $groupId, int $studentId, Request $request): Response
    {
        $success = $this->groupManager->addStudent($groupId, $studentId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t add this user as student'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{groupId}/remove-student/{studentId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'studentId' => '\d+'])]
    public function removeStudent(int $groupId, int $studentId, Request $request): Response
    {
        $success = $this->groupManager->removeStudent($groupId, $studentId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t remove this user as student'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }
}