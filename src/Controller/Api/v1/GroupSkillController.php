<?php

namespace App\Controller\Api\v1;

use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/group')]
class GroupSkillController extends AbstractController
{
    private GroupManager $groupManager;

    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    #[Route(path: '/{groupId}/add-skill/{skillId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'skillId' => '\d+'])]
    public function addSkill(int $groupId, int $skillId, Request $request): Response
    {
        $success = $this->groupManager->addSkill($groupId, $skillId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t add this skill'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{groupId}/remove-skill/{skillId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'skillId' => '\d+'])]
    public function removeSkill(int $groupId, int $skillId, Request $request): Response
    {
        $success = $this->groupManager->removeSkill($groupId, $skillId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t remove this skill'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }
}