<?php

namespace App\Controller\Api\v1;

use App\Entity\GroupSuggestion;
use App\Manager\GroupSuggestionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/group-suggestion')]
class GroupSuggestionController extends AbstractController
{
    private GroupSuggestionManager $groupSuggestionManager;

    public function __construct(GroupSuggestionManager $groupSuggestionManager)
    {
        $this->groupSuggestionManager = $groupSuggestionManager;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getGroupsSuggestions(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");

        $groupSuggestions = $this->groupSuggestionManager->getGroupSuggestions($page, $perPage);

        [$data, $code] = empty($groupSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'groups_suggestion' => array_map(
                static fn (GroupSuggestion $groupSuggestion) => ['group_suggestion' => $groupSuggestion->toArray()],
                $groupSuggestions)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getGroupSuggestionById(int $id): Response
    {
        $groupSuggestions = $this->groupSuggestionManager->getGroupSuggestionById($id);

        [$data, $code] = empty($groupSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'group_suggestion' => $groupSuggestions->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/for-student/{studentId}', methods: ['GET'], requirements: ['studentId' => '\d+'])]
    public function getGroupSuggestionByStudentId(int $studentId): Response
    {
        $groupSuggestions = $this->groupSuggestionManager->getGroupSuggestionByStudentId($studentId);

        [$data, $code] = empty($groupSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'groups_suggestion' => array_map(
                static fn (GroupSuggestion $groupSuggestion) => ['group_suggestion' => $groupSuggestion->toArray()],
                $groupSuggestions)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteGroupSuggestion(int $id): Response
    {
        $success = $this->groupSuggestionManager->deleteGroupSuggestion($id);

        [$data, $code] = $success ?
            [['success' => true], Response::HTTP_OK] :
            [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }
}