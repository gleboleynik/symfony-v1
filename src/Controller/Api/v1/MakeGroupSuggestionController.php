<?php

namespace App\Controller\Api\v1;

use App\DTO\MakeGroupSuggestionsDTO;
use App\Manager\UserManager;
use App\Service\GroupSuggestionsService;
use App\Service\QueryPublishService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MakeGroupSuggestionController extends AbstractController
{
    private UserManager $userManager;

    private QueryPublishService $queryPublishService;

    private SerializerInterface $serializer;

    private ValidatorInterface $validator;

    private GroupSuggestionsService $groupSuggestionsService;

    public function __construct(
        UserManager $userManager,
        QueryPublishService $queryPublishService,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        GroupSuggestionsService $groupSuggestionsService
    )
    {
        $this->userManager = $userManager;
        $this->queryPublishService = $queryPublishService;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->groupSuggestionsService = $groupSuggestionsService;
    }

    #[Route(path: '/api/v1/make-group-suggestion/', methods: ['POST'])]
    public function makeGroupsSuggestion(Request $request): Response
    {
        // send to query
        $makeGroupSuggestionDTO = $this->serializer->deserialize($request->getContent(), MakeGroupSuggestionsDTO::class, 'json');
        $errors = $this->validator->validate($makeGroupSuggestionDTO);

        $user = $this->userManager->getUserById($makeGroupSuggestionDTO->userId);
        if ($user !== null && $user->isStudent() && $errors->count() === 0) {
            $result = $this->queryPublishService->publishToExchange(QueryPublishService::MAKE_GROUP_SUGGESTIONS, $this->serializer->serialize($makeGroupSuggestionDTO, 'json'));
            [$data, $code] = [['success' => $result], $result ? Response::HTTP_OK : Response::HTTP_INTERNAL_SERVER_ERROR];
        } else {
            [$data, $code] = [['success' => false], Response::HTTP_NOT_FOUND];
        }

        return new JsonResponse($data, $code);
    }
}