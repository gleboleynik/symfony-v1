<?php

namespace App\Controller\Api\v1;

use App\Entity\TeacherSuggestion;
use App\Manager\TeacherSuggestionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/teacher-suggestion')]
class TeacherSuggestionController extends AbstractController
{
    private TeacherSuggestionManager $teacherSuggestionManager;

    public function __construct(TeacherSuggestionManager $teacherSuggestionManager)
    {
        $this->teacherSuggestionManager = $teacherSuggestionManager;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getTeacherSuggestions(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");

        $teacherSuggestions = $this->teacherSuggestionManager->getTeacherSuggestions($page, $perPage);

        [$data, $code] = empty($teacherSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'teacher_suggestions' => array_map(
                static fn (TeacherSuggestion $teacherSuggestion) => ['teacher_suggestion' => $teacherSuggestion->toArray()],
                $teacherSuggestions)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getTeacherSuggestionById(int $id): Response
    {
        $teacherSuggestions = $this->teacherSuggestionManager->getTeacherSuggestionById($id);

        [$data, $code] = empty($teacherSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'teacher_suggestion' => $teacherSuggestions->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/for-group/{groupId}', methods: ['GET'], requirements: ['groupId' => '\d+'])]
    public function getTeacherSuggestionByStudentId(int $groupId): Response
    {
        $teacherSuggestions = $this->teacherSuggestionManager->getTeacherSuggestionByGroupId($groupId);

        [$data, $code] = empty($teacherSuggestions) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'teacher_suggestions' => array_map(
                static fn (TeacherSuggestion $teacherSuggestion) => ['teacher_suggestions' => $teacherSuggestion->toArray()],
                $teacherSuggestions)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteTeacherSuggestion(int $id): Response
    {
        $success = $this->teacherSuggestionManager->deleteTeacherSuggestion($id);

        [$data, $code] = $success ?
            [['success' => true], Response::HTTP_OK] :
            [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }
}