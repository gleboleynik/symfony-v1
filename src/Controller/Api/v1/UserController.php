<?php

namespace App\Controller\Api\v1;

use App\DTO\SaveUserDTO;
use App\DTO\UpdateUserDTO;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: 'api/v1/user')]
class UserController extends AbstractController
{
    private UserManager $userManager;

    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    public function __construct(UserManager $userManager, ValidatorInterface $validator, SerializerInterface  $serializer)
    {
        $this->userManager = $userManager;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getUsers(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");
        $userType = $request->get('user_type', '');

        $users = $this->userManager->getUsersByType($page, $perPage, $userType);

        [$data, $code] = empty($users) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'users' => array_map(
                static fn (User $user) => ['user' => $user->toArray()],
                $users)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getUserById(int $id): Response
    {
        $user = $this->userManager->getUserById($id);

        [$data, $code] = empty($user) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'user' => $user->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '', methods: ['POST'])]
    public function addUser(Request $request): Response
    {
        $saveUserDTO = $this->serializer->deserialize($request->getContent(), SaveUserDTO::class, 'json');
        $errors = $this->validator->validate($saveUserDTO);

        $userId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $userId = $this->userManager->saveUserFromDTO(new User(), $saveUserDTO);
        }

        [$data, $code] = $userId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'userId' => $userId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['POST'], requirements: ['id' => '\d+'])]
    public function updateUser(User $user, Request $request): Response
    {
        $updateUserDTO = $this->serializer->deserialize($request->getContent(), UpdateUserDTO::class, 'json');
        $errors = $this->validator->validate($updateUserDTO);

        $userId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $user = $this->userManager->updateUserFromDTO($user->getId(), $updateUserDTO);
            $userId = $user->getId();
        }

        [$data, $code] = $userId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'userId' => $userId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteUser(int $id): Response
    {
        $success = $this->userManager->deleteUser($id);

        [$data, $code] = $success ?
        [['success' => true], Response::HTTP_OK] :
        [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }
}