<?php

namespace App\Controller\Api\v1;

use App\DTO\SaveLessonDTO;
use App\DTO\UpdateLessonDTO;
use App\Entity\Lesson;
use App\Manager\LessonManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: 'api/v1/lesson')]
class LessonController extends AbstractController
{
    private LessonManager $lessonManager;

    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    public function __construct(LessonManager $lessonManager, ValidatorInterface $validator, SerializerInterface  $serializer)
    {
        $this->lessonManager = $lessonManager;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getLessons(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");

        $lessons = $this->lessonManager->getLessons($page, $perPage);

        [$data, $code] = empty($lessons) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'lessons' => array_map(
                static fn (Lesson $lesson) => ['lesson' => $lesson->toArray()],
                $lessons)], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getLessonById(int $id): Response
    {
        $lesson = $this->lessonManager->getLessonById($id);

        [$data, $code] = empty($lesson) ?
            [['success' => true], Response::HTTP_NO_CONTENT] :
            [['success' => true, 'lesson' => $lesson->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '', methods: ['POST'])]
    public function addLesson(Request $request): Response
    {
        $saveLessonDTO = $this->serializer->deserialize($request->getContent(), SaveLessonDTO::class, 'json');
        $errors = $this->validator->validate($saveLessonDTO);

        $lessonId = null;
        $errorsString = '';
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $lessonId = $this->lessonManager->saveLessonFromDTO(new Lesson(), $saveLessonDTO);
        }

        [$data, $code] = $lessonId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'lessonId' => $lessonId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{lessonId}/add-teacher/{teacherId}', methods: ['POST'], requirements: ['lessonId' => '\d+', 'teacherId' => '\d+'])]
    public function addTeacher(int $lessonId, int $teacherId, Request $request): Response
    {
        $success = $this->lessonManager->addTeacher($lessonId, $teacherId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t add this user as teacher'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['POST'], requirements: ['id' => '\d+'])]
    public function updateLesson(Lesson $lesson, Request $request): Response
    {
        $updateLessonDTO = $this->serializer->deserialize($request->getContent(), UpdateLessonDTO::class, 'json');
        $errors = $this->validator->validate($updateLessonDTO);

        $lessonId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $lesson = $this->lessonManager->updateLessonFromDTO($lesson->getId(), $updateLessonDTO);
            $lessonId = $lesson->getId();
        }

        [$data, $code] = $lessonId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'lessonId' => $lessonId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteUser(int $id): Response
    {
        $success = $this->lessonManager->deleteLesson($id);

        [$data, $code] = $success ?
            [['success' => true], Response::HTTP_OK] :
            [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }
}