<?php

namespace App\Controller\Api\v1;

use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/group')]
class GroupLessonController extends AbstractController
{
    private GroupManager $groupManager;

    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    #[Route(path: '/{groupId}/add-lesson/{lessonId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'lessonId' => '\d+'])]
    public function addLesson(int $groupId, int $lessonId, Request $request): Response
    {
        $success = $this->groupManager->addLesson($groupId, $lessonId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t add this lesson'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{groupId}/remove-lesson/{lessonId}', methods: ['POST'], requirements: ['groupId' => '\d+', 'lessonId' => '\d+'])]
    public function removeLesson(int $groupId, int $lessonId, Request $request): Response
    {
        $success = $this->groupManager->removeLesson($groupId, $lessonId);

        [$data, $code] = !$success ?
            [['success' => false, 'errors' => 'Can\'t remove this lesson'], Response::HTTP_BAD_REQUEST] :
            [['success' => true], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }
}