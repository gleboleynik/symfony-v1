<?php

namespace App\Controller\Api\v1;

use App\DTO\SaveGroupDTO;
use App\DTO\SaveGroupFormDTO;
use App\DTO\UpdateGroupDTO;
use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

#[Route(path: 'api/v1/group')]
class GroupController extends AbstractController
{
    private GroupManager $groupManager;

    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    private Environment $twig;

    public function __construct(GroupManager $groupManager, ValidatorInterface $validator, SerializerInterface  $serializer, Environment $twig)
    {
        $this->groupManager = $groupManager;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->twig = $twig;
    }

    #[Route(path: '', methods: ['GET'])]
    public function getGroups(Request $request): Response
    {
        $page = $request->get('page', "0");
        $perPage = $request->get('perPage', "20");

        $groups = $this->groupManager->getGroups($page, $perPage);

        $data = ['success' => true];
        $code = Response::HTTP_NO_CONTENT;

        if(!empty($groups)) {
            $data['groups'] = array_map(static fn (Group $group) => ['group' => $group->toArray()], $groups);
            $code = Response::HTTP_OK;
        }

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getGroupById(int $id): Response
    {
        $group = $this->groupManager->getGroupById($id);

        [$data, $code] = empty($group) ?
            [['success' => true], Response::HTTP_NOT_FOUND] :
            [['success' => true, 'group' => $group->toArray()], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '', methods: ['POST'])]
    public function addGroup(Request $request): Response
    {
        $saveGroupDTO = $this->serializer->deserialize($request->getContent(), SaveGroupDTO::class, 'json');
        $errors = $this->validator->validate($saveGroupDTO);

        $groupId = null;
        $errorsString = '';
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $groupId = $this->groupManager->saveGroupFromDTO(new Group(), $saveGroupDTO);
        }

        [$data, $code] = $groupId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'groupId' => $groupId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['POST'], requirements: ['id' => '\d+'])]
    public function updateGroup(Group $group, Request $request): Response
    {
        $updateGroupDTO = $this->serializer->deserialize($request->getContent(), UpdateGroupDTO::class, 'json');
        $errors = $this->validator->validate($updateGroupDTO);

        $groupId = null;
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
        } else {
            $group = $this->groupManager->updateGroupFromDTO($group->getId(), $updateGroupDTO);
            $groupId = $group->getId();
        }

        [$data, $code] = $groupId === null ?
            [['success' => false, 'errors' => $errorsString], Response::HTTP_BAD_REQUEST] :
            [['success' => true, 'groupId' => $groupId], Response::HTTP_OK];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function deleteGroup(int $id): Response
    {
        $success = $this->groupManager->deleteGroup($id);

        [$data, $code] = $success ?
            [['success' => true], Response::HTTP_OK] :
            [['success' => false], Response::HTTP_BAD_REQUEST];

        return new JsonResponse($data, $code);
    }

    #[Route(path: '/form', methods: ['GET'])]
    public function getSaveForm(): Response
    {
        $form = $this->groupManager->getSaveForm();

        $content = $this->twig->render('form.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($content);
    }

    #[Route(path: '/form', methods: ['POST'])]
    public function saveGroupForm(Request $request): Response
    {
        $form = $this->groupManager->getSaveForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $groupId = $this->groupManager->saveGroupFormFromDTO(new Group(), new SaveGroupFormDTO($form->getData()));
            [$data, $code] = ($groupId === null) ? [['success' => false], 400] : [['id' => $groupId], 200];

            return new JsonResponse($data, $code);
        }

        $content = $this->twig->render('form.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($content);
    }

    #[Route(path: '/form/{id}', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function getUpdateFormAction(int $id): Response
    {
        $form = $this->groupManager->getUpdateForm($id);
        if ($form === null) {
            return new JsonResponse(['message' => "User with ID $id not found"], 404);
        }
        $content = $this->twig->render('form.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($content);
    }

    #[Route(path: '/form/{id}', requirements: ['id' => '\d+'], methods: ['PATCH'])]
    public function updateUserFormAction(Request $request, int $id): Response
    {
        $form = $this->groupManager->getUpdateForm($id);
        if ($form === null) {
            return new JsonResponse(['message' => "Group with ID $id not found"], 404);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->groupManager->updateUserFormFromDTO($id, $form->getData());

            return new JsonResponse(['success' => $result], $result ? 200 : 400);
        }
        $content = $this->twig->render('form.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($content);
    }
}