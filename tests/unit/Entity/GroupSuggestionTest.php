<?php

namespace UnitTests\Entity;

use App\Entity\Group;
use App\Entity\GroupSuggestion;
use App\Entity\Skill;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\PhpUnit\ClockMock;

class GroupSuggestionTest extends TestCase
{
    private const NOW_TIME = '@now';

    public function PositiveGroupSuggestionDataProvider(): array
    {
        $positiveWithoutSkills = $this->makeExpectedGroupSuggestion(1, true, true, []);
        $positiveWithLearnedSkills = $this->makeExpectedGroupSuggestion(1, true, true, [
            'learned_skills' => [1,2]
        ]);
        $positiveWithNotLearnedSkills = $this->makeExpectedGroupSuggestion(1, true, true, [
            'not_learned_skills' => [3]
        ]);
        $positiveWithAdditionalLearnedSkills = $this->makeExpectedGroupSuggestion(1, true, true, [
            'additional_learned_skills' => [3]
        ]);
        $positiveWithAllLearnedSkills = $this->makeExpectedGroupSuggestion(1, true, true, [
            'learned_skills' => [1],
            'not_learned_skills' => [2,3],
            'additional_learned_skills' => [4,5,6]
        ]);


        return [
            'positiveWithoutSkills' => [
                $this->makeGroupSuggestion($positiveWithoutSkills),
                $positiveWithoutSkills
            ],
            'positiveWithLearnedSkills' => [
                $this->makeGroupSuggestion($positiveWithLearnedSkills),
                $positiveWithLearnedSkills
            ],
            'positiveWithNotLearnedSkills' => [
                $this->makeGroupSuggestion($positiveWithNotLearnedSkills),
                $positiveWithNotLearnedSkills
            ],
            'positiveWithAdditionalLearnedSkills' => [
                $this->makeGroupSuggestion($positiveWithAdditionalLearnedSkills),
                $positiveWithAdditionalLearnedSkills
            ],
            'positiveWithAllSkills' => [
                $this->makeGroupSuggestion($positiveWithAllLearnedSkills),
                $positiveWithAllLearnedSkills
            ]
        ];
    }

    public function NegativeGroupSuggestionDataProvider(): array
    {
        return [
            'negativeWithoutGroup' => [$this->makeExpectedGroupSuggestion(1, false, true, [])],
            'negativeWithoutStudent' => [$this->makeExpectedGroupSuggestion(1, true, false, [])]
        ];
    }

    /**
     * @dataProvider PositiveGroupSuggestionDataProvider
     * @group time-sensitive
     */
    public function testPositiveCases(GroupSuggestion $groupSuggestion, array $expected): void
    {
        ClockMock::register(GroupSuggestion::class);
        if ($expected['created_at'] === self::NOW_TIME) {
            $expected['created_at'] = DateTime::createFromFormat('U',(string)time())->format('Y-m-d H:i:s');
            $expected['updated_at'] = DateTime::createFromFormat('U',(string)time())->format('Y-m-d H:i:s');
        }

        $actual = $groupSuggestion->toArray();

        self::assertSame($expected, $actual, 'GroupSuggestion::toArray should return correct result');
    }

    /**
     * @dataProvider NegativeGroupSuggestionDataProvider
     */
    public function testNegativeCases($expected): void
    {

        $this->expectException(\TypeError::class);

        $this->makeGroupSuggestion($expected);

    }

    private function makeExpectedGroup(): array
    {
        return [
            'group_id' => 10,
            'group_code' => 'TEST_GROUP',
            'group_name' => 'test group',
        ];
    }

    private function makeGroup(array $data): Group
    {
        $group = new Group();
        $group->setId($data['group_id']);
        $group->setCode($data['group_code']);
        $group->setName($data['group_name']);

        return $group;
    }

    private function makeExpectedStudent(bool $isExpected): ?array
    {
        $expectedStudent = null;

        if($isExpected) {
            $expectedStudent = [
                'student_id' => 20,
                'student_fullname' => 'Test User',
            ];
        }

        return $expectedStudent;
    }

    private function makeStudent(array $data): User
    {
        $student = new User();
        $student->setId($data['student_id']);
        $names = explode(' ', $data['student_fullname']);
        $student->setName(array_shift($names));
        $student->setSurname(array_shift($names));

        return $student;
    }

    private function makeExpectedSkill(int $skillNumber): array
    {
        return [
            'skill_id' => $skillNumber,
            'skill_code' => 'Skill_'.$skillNumber,
            'skill_name' => 'SkillName_'.$skillNumber,
            'skill_level' => 1
        ];
    }

    private function makeSkill($data): Skill
    {
        $skill = new Skill();
        $skill->setId($data['skill_id']);
        $skill->setCode($data['skill_code']);
        $skill->setName($data['skill_name']);
        $skill->setLevel($data['skill_level']);

        return $skill;
    }

    private function makeExpectedGroupSuggestion(int $id, bool $isMakeGroup, bool $isMakeStudent = null, array $skillsData = []): array
    {
        $expectedGroupSuggestion = [
            'id' => $id,
            'group' => null,
            'learned_skills' => [],
            'not_learned_skills' => [],
            'additional_learned_skills' => [],
            'student' => null,
            'created_at' => self::NOW_TIME,
            'updated_at' => self::NOW_TIME,
        ];

        if($isMakeGroup) {
            $expectedGroupSuggestion['group'] = $this->makeExpectedGroup(true);
        }

        if($isMakeStudent) {
            $expectedGroupSuggestion['student'] = $this->makeExpectedStudent(true);
        }

        if($skillsData !== null) {
            foreach ($skillsData as $skillsType => $skillsList) {
                foreach ($skillsList as $skillID) {
                    $expectedGroupSuggestion[$skillsType][] = $this->makeExpectedSkill($skillID);
                }
            }
        }

        return $expectedGroupSuggestion;
    }

    private function makeGroupSuggestion($data): GroupSuggestion
    {
        $groupSuggestion = new GroupSuggestion();
        $groupSuggestion->setId($data['id']);

        $group = $data['group'] !== null ? $this->makeGroup($data['group']) : null;
        $groupSuggestion->setGroup($group);

        $student = $data['student'] !== null ? $this->makeStudent($data['student']) : null;
        $groupSuggestion->setStudent($student);

        if(!empty($data['learned_skills'])) {
            $learnedSkills = new ArrayCollection();
            foreach($data['learned_skills'] as $skillData) {
                $learnedSkills->add($this->makeSkill($skillData));
            }
            $groupSuggestion->setLearnedSkills($learnedSkills);
        }

        if(!empty($data['not_learned_skills'])) {
            $notLearnedSkills = new ArrayCollection();
            foreach($data['not_learned_skills'] as $skillData) {
                $notLearnedSkills->add($this->makeSkill($skillData));
            }
            $groupSuggestion->setNotLearnedSkills($notLearnedSkills);
        }

        if(!empty($data['additional_learned_skills'])) {
            $additionalLearnedSkills = new ArrayCollection();
            foreach($data['additional_learned_skills'] as $skillData) {
                $additionalLearnedSkills->add($this->makeSkill($skillData));
            }
            $groupSuggestion->setAdditionalLearnedSkills($additionalLearnedSkills);
        }

        $groupSuggestion->setCreatedAt();
        $groupSuggestion->setUpdatedAt();

        return $groupSuggestion;
    }
}