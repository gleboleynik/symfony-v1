<?php

namespace UnitTests\Service;

use App\Entity\Group;
use App\Entity\GroupSuggestion;
use App\Entity\Skill;
use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\GroupSuggestionManager;
use App\Service\GroupSuggestionsService;
use DateTime;
use Mockery;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\PhpUnit\ClockMock;

class GroupSuggestionServiceTest extends TestCase
{
    private array $skills = [];

    private GroupManager $groupManager;

    private GroupSuggestionManager $groupSuggestionManager;

    private const NOW_TIME = '@now';

    protected function setUp(): void
    {
        $this->groupManager = Mockery::mock(GroupManager::class);
        $this->groupSuggestionManager = Mockery::mock(GroupSuggestionManager::class);
    }

    public function MakeGroupSuggestionDataProvider(): array
    {
        $this->fillSkillsProperty();

        return [
            'OneSkillNotSuitable' => [
                [1],
                50.0,
                null
            ],
            'TwoSkillsNotSuitable' => [
                [1,5],
                50.0,
                null
            ],
            'ThreeSkillsSuitable' => [
                [1,2,5],
                50.0,
                $this->makeExpectedGroupSuggestion(null, [
                    'learned_skills' => [1,2],
                    'not_learned_skills' => [3,4],
                    'additional_learned_skills' => [5]
                ])
            ],
            'FiveSkillsSuitable' => [
                [1,2,3,4,5],
                100.0,
                $this->makeExpectedGroupSuggestion(null, [
                    'learned_skills' => [1,2,3,4],
                    'not_learned_skills' => [],
                    'additional_learned_skills' => [5]
                ])
            ]
        ];
    }

    /**
     * @dataProvider MakeGroupSuggestionDataProvider
     * @group time-sensitive
     */
    public function testMakeGroupSuggestion(array $groupSkills, float $percentLearnSkills, ?array $expected): void
    {
        $this->fillSkillsProperty();
        $groupSuggestionsService = new GroupSuggestionsService($this->groupManager, $this->groupSuggestionManager);

        $student = $this->makeStudent();
        $group = $this->makeGroup();
        foreach ($groupSkills as $skillID) {
            $group->addSkill($this->skills[$skillID]);
        }

        ClockMock::register(GroupSuggestion::class);
        if ($expected !== null && $expected['created_at'] === self::NOW_TIME) {
            $expected['created_at'] = DateTime::createFromFormat('U',(string)time())->format('Y-m-d H:i:s');
            $expected['updated_at'] = DateTime::createFromFormat('U',(string)time())->format('Y-m-d H:i:s');
        }

        $reflection = new \ReflectionClass(get_class($groupSuggestionsService));
        $method = $reflection->getMethod('makeGroupSuggestion');
        $method->setAccessible(true);
        $groupSuggestion = $method->invokeArgs($groupSuggestionsService, [$student, $group, $percentLearnSkills]);

        $actual = $groupSuggestion;
        if($actual !== null) {
            $groupSuggestion->setCreatedAt(DateTime::createFromFormat('U',(string)time()));
            $groupSuggestion->setUpdatedAt(DateTime::createFromFormat('U',(string)time()));
            $actual = $groupSuggestion->toArray();
            $actual['learned_skills'] = array_values($actual['learned_skills']);
            $actual['not_learned_skills'] = array_values($actual['not_learned_skills']);
            $actual['additional_learned_skills'] = array_values($actual['additional_learned_skills']);
        }

        $this->assertSame($expected, $actual);
    }

    private function makeExpectedGroupSuggestion(?int $id, array $skillsData = []): array
    {
        $group = $this->makeGroup();
        $student = $this->makeStudent();

        $expectedGroupSuggestion = [
            'id' => $id,
            'group' => [
                'group_id' => $group->getId(),
                'group_code' => $group->getCode(),
                'group_name' => $group->getName(),
            ],
            'learned_skills' => [],
            'not_learned_skills' => [],
            'additional_learned_skills' => [],
            'student' => [
                'student_id' => $student->getId(),
                'student_fullname' => $student->getName(). ' ' .$student->getSurname(),
            ],
            'created_at' => self::NOW_TIME,
            'updated_at' => self::NOW_TIME,
        ];

        if($skillsData !== null) {
            foreach ($skillsData as $skillsType => $skillsList) {
                foreach ($skillsList as $skillID) {
                    $expectedGroupSuggestion[$skillsType][] = $this->makeExpectedSkill($skillID);
                }
            }
        }

        return $expectedGroupSuggestion;
    }

    private function makeGroup(): Group
    {
        $group = new Group();
        $group->setId(100);
        $group->setCode('TEST GROUP');
        $group->setName('Test group');

        return $group;
    }

    private function makeStudent(): User
    {
        $student = new User();
        $student->setId(99);
        $student->setName('Test');
        $student->setSurname('Name');
        $student->addSkill($this->skills[1]);
        $student->addSkill($this->skills[2]);
        $student->addSkill($this->skills[3]);
        $student->addSkill($this->skills[4]);

        return $student;
    }

    private function makeExpectedSkill(int $skillNumber): array
    {
        $skill = $this->skills[$skillNumber];
        return [
            'skill_id' => $skill->getId(),
            'skill_code' => $skill->getCode(),
            'skill_name' => $skill->getName(),
            'skill_level' => $skill->getLevel()
        ];
    }

    private function makeSkill(int $skillNumber): Skill
    {
        $skill = new Skill();
        $skill->setId($skillNumber);
        $skill->setCode('SKILL_'.$skillNumber);
        $skill->setName('Skill_'.$skillNumber);
        $skill->setLevel(1);

        return $skill;
    }

    private function fillSkillsProperty(): void
    {
        for($i = 1; $i <= 6; $i++) {
            $this->skills[$i] = $this->makeSkill($i);
        }
    }
}