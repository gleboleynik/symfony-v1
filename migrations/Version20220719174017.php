<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719174017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX CONCURRENTLY user__login__idx ON "user" (login)');
        $this->addSql('CREATE INDEX CONCURRENTLY user__is_student__idx ON "user" (is_student)');
        $this->addSql('CREATE INDEX CONCURRENTLY user__is_teacher__idx ON "user" (is_teacher)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX user__login__idx');
        $this->addSql('DROP INDEX user__is_student__idx');
        $this->addSql('DROP INDEX user__is_teacher__idx');
    }
}
