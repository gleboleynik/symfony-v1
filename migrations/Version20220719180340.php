<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719180340 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX CONCURRENTLY IDX_B9EC4E89A76ED395 ON skills_users (user_id)');
        $this->addSql('CREATE INDEX CONCURRENTLY IDX_B9EC4E895585C142 ON skills_users (skill_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX IDX_B9EC4E895585C142');
        $this->addSql('DROP INDEX IDX_B9EC4E89A76ED395');
    }
}
