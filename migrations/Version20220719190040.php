<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719190040 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "lesson" (id BIGSERIAL NOT NULL, teacher_id BIGINT DEFAULT NULL, skill_id BIGINT DEFAULT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, duration INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F87474F341807E1D ON "lesson" (teacher_id)');
        $this->addSql('CREATE INDEX IDX_F87474F35585C142 ON "lesson" (skill_id)');
        $this->addSql('ALTER TABLE "lesson" ADD CONSTRAINT lesson__teacher_id__fk FOREIGN KEY (teacher_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "lesson" ADD CONSTRAINT lesson__skill_id__fk FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE "lesson"');
    }
}
