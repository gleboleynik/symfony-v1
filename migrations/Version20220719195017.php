<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719195017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "teacher_suggestion" (id BIGSERIAL NOT NULL, group_id BIGINT DEFAULT NULL, teacher_id BIGINT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_579F51BBFE54D947 ON "teacher_suggestion" (group_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_579F51BB41807E1D ON "teacher_suggestion" (teacher_id)');
        $this->addSql('CREATE TABLE teacher_suggestion__lessons (teacher_suggestion__id BIGINT NOT NULL, lesson_id BIGINT NOT NULL, PRIMARY KEY(teacher_suggestion__id, lesson_id))');
        $this->addSql('CREATE INDEX IDX_3E7C2F468FF10354 ON teacher_suggestion__lessons (teacher_suggestion__id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E7C2F46CDF80196 ON teacher_suggestion__lessons (lesson_id)');
        $this->addSql('ALTER TABLE "teacher_suggestion" ADD CONSTRAINT teacher_suggestion__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "teacher_suggestion" ADD CONSTRAINT teacher_suggestion__teacher_id__fk FOREIGN KEY (teacher_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE teacher_suggestion__lessons ADD CONSTRAINT teacher_suggestion__lessons__teacher_suggestion__id__fk FOREIGN KEY (teacher_suggestion__id) REFERENCES "teacher_suggestion" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE teacher_suggestion__lessons ADD CONSTRAINT teacher_suggestion__lessons__lesson_id__fk FOREIGN KEY (lesson_id) REFERENCES "lesson" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE teacher_suggestion DROP CONSTRAINT teacher_suggestion__group_id__fk');
        $this->addSql('ALTER TABLE teacher_suggestion DROP CONSTRAINT teacher_suggestion__teacher_id__fk');
        $this->addSql('ALTER TABLE teacher_suggestion__lessons DROP CONSTRAINT teacher_suggestion__lessons__teacher_suggestion__id__fk');
        $this->addSql('ALTER TABLE teacher_suggestion__lessons DROP CONSTRAINT teacher_suggestion__lessons__lesson_id__fk');
        $this->addSql('DROP TABLE "teacher_suggestion"');
        $this->addSql('DROP TABLE teacher_suggestion__lessons');
    }
}
