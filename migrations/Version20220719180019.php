<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719180019 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "skill" (id BIGSERIAL NOT NULL, code VARCHAR(20) NOT NULL, name VARCHAR(100) NOT NULL, level INT DEFAULT NULL, lessons_count INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5E3DE47777153098 ON "skill" (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5E3DE4775E237E06 ON "skill" (name)');
        $this->addSql('CREATE TABLE skills_users (user_id BIGINT NOT NULL, skill_id BIGINT NOT NULL, PRIMARY KEY(user_id, skill_id))');
        $this->addSql('ALTER TABLE skills_users ADD CONSTRAINT skills_users__user_id__fk FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE skills_users ADD CONSTRAINT skills_users__skill_id_fk FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE skills_users DROP CONSTRAINT skills_users__skill_id_fk');
        $this->addSql('ALTER TABLE skills_users DROP CONSTRAINT skills_users__user_id__fk');
        $this->addSql('DROP TABLE "skill"');
        $this->addSql('DROP TABLE skills_users');
    }
}
