<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220728171352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER INDEX idx_f87474f341807e1d RENAME TO lesson__teacher_id__idx');
        $this->addSql('ALTER INDEX uniq_6dc044c577153098 RENAME TO group__code__unique_idx');
        $this->addSql('ALTER INDEX uniq_8d93d649aa08cb10 RENAME TO user__login__unique_idx');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
