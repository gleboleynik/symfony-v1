<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220902075046 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE suggestion_additional_learned_skills (group_suggestion__id BIGINT NOT NULL, skill_id BIGINT NOT NULL, PRIMARY KEY(group_suggestion__id, skill_id))');
        $this->addSql('CREATE INDEX IDX_B02F6EDE40DC3E8D ON suggestion_additional_learned_skills (group_suggestion__id)');
        $this->addSql('CREATE INDEX IDX_B02F6EDE5585C142 ON suggestion_additional_learned_skills (skill_id)');
        $this->addSql('ALTER TABLE suggestion_additional_learned_skills ADD CONSTRAINT suggestion_additional_learned_skills__group_suggestion_id__fk FOREIGN KEY (group_suggestion__id) REFERENCES "group_suggestion" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE suggestion_additional_learned_skills ADD CONSTRAINT suggestion_additional_learned_skills__skill_id__fk FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX uniq_a4d92a9fe54d947');
        $this->addSql('DROP INDEX uniq_a4d92a9cb944f1a');
        $this->addSql('CREATE INDEX IDX_A4D92A9FE54D947 ON group_suggestion (group_id)');
        $this->addSql('CREATE INDEX IDX_A4D92A9CB944F1A ON group_suggestion (student_id)');
        $this->addSql('DROP INDEX uniq_7c08b9685585c142');
        $this->addSql('CREATE INDEX IDX_7C08B9685585C142 ON suggestion_learned_skills (skill_id)');
        $this->addSql('DROP INDEX uniq_d21081e45585c142');
        $this->addSql('CREATE INDEX IDX_D21081E45585C142 ON suggestion_not_learned_skills (skill_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE suggestion_additional_learned_skills');
        $this->addSql('DROP INDEX IDX_A4D92A9FE54D947');
        $this->addSql('DROP INDEX IDX_A4D92A9CB944F1A');
        $this->addSql('CREATE UNIQUE INDEX uniq_a4d92a9fe54d947 ON "group_suggestion" (group_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_a4d92a9cb944f1a ON "group_suggestion" (student_id)');
        $this->addSql('DROP INDEX IDX_7C08B9685585C142');
        $this->addSql('CREATE UNIQUE INDEX uniq_7c08b9685585c142 ON suggestion_learned_skills (skill_id)');
        $this->addSql('DROP INDEX IDX_D21081E45585C142');
        $this->addSql('CREATE UNIQUE INDEX uniq_d21081e45585c142 ON suggestion_not_learned_skills (skill_id)');
    }
}
