<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719195603 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "group_suggestion" (id BIGSERIAL NOT NULL, group_id BIGINT DEFAULT NULL, student_id BIGINT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A4D92A9FE54D947 ON "group_suggestion" (group_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A4D92A9CB944F1A ON "group_suggestion" (student_id)');
        $this->addSql('CREATE TABLE suggestion_learned_skills (group_suggestion__id BIGINT NOT NULL, skill_id BIGINT NOT NULL, PRIMARY KEY(group_suggestion__id, skill_id))');
        $this->addSql('CREATE INDEX IDX_7C08B96840DC3E8D ON suggestion_learned_skills (group_suggestion__id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7C08B9685585C142 ON suggestion_learned_skills (skill_id)');
        $this->addSql('CREATE TABLE suggestion_not_learned_skills (group_suggestion__id BIGINT NOT NULL, skill_id BIGINT NOT NULL, PRIMARY KEY(group_suggestion__id, skill_id))');
        $this->addSql('CREATE INDEX IDX_D21081E440DC3E8D ON suggestion_not_learned_skills (group_suggestion__id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D21081E45585C142 ON suggestion_not_learned_skills (skill_id)');
        $this->addSql('ALTER TABLE "group_suggestion" ADD CONSTRAINT group_suggestion__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "group_suggestion" ADD CONSTRAINT group_suggestion__student_id__fk FOREIGN KEY (student_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE suggestion_learned_skills ADD CONSTRAINT suggestion_learned_skills__group_suggestion__id__fk FOREIGN KEY (group_suggestion__id) REFERENCES "group_suggestion" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE suggestion_learned_skills ADD CONSTRAINT suggestion_learned_skills__skill_id__fk FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE suggestion_not_learned_skills ADD CONSTRAINT suggestion_not_learned_skills__group_suggestion__id__fk FOREIGN KEY (group_suggestion__id) REFERENCES "group_suggestion" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE suggestion_not_learned_skills ADD CONSTRAINT suggestion_not_learned_skills__skill_id__fk FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE suggestion_learned_skills DROP CONSTRAINT suggestion_learned_skills__group_suggestion__id__fk');
        $this->addSql('ALTER TABLE suggestion_learned_skills DROP CONSTRAINT suggestion_learned_skills__skill_id__fk');
        $this->addSql('ALTER TABLE suggestion_not_learned_skills DROP CONSTRAINT suggestion_not_learned_skills__group_suggestion__id__fk');
        $this->addSql('ALTER TABLE suggestion_not_learned_skills DROP CONSTRAINT suggestion_not_learned_skills__skill_id__fk');
        $this->addSql('DROP TABLE "group_suggestion"');
        $this->addSql('DROP TABLE suggestion_learned_skills');
        $this->addSql('DROP TABLE suggestion_not_learned_skills');
    }
}
