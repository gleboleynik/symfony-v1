<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719192932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "group" (id BIGSERIAL NOT NULL, code VARCHAR(20) NOT NULL, name VARCHAR(100) NOT NULL, min_students_count INT NOT NULL, max_students_count INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6DC044C577153098 ON "group" (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6DC044C55E237E06 ON "group" (name)');
        $this->addSql('CREATE TABLE skills_groups (skill_id BIGINT NOT NULL, group_id BIGINT NOT NULL, PRIMARY KEY(skill_id, group_id))');
        $this->addSql('CREATE INDEX IDX_BD7237C35585C142 ON skills_groups (skill_id)');
        $this->addSql('CREATE INDEX IDX_BD7237C3FE54D947 ON skills_groups (group_id)');
        $this->addSql('CREATE TABLE groups_students (user_id BIGINT NOT NULL, group_id BIGINT NOT NULL, PRIMARY KEY(user_id, group_id))');
        $this->addSql('CREATE INDEX IDX_1F7B776DA76ED395 ON groups_students (user_id)');
        $this->addSql('CREATE INDEX IDX_1F7B776DFE54D947 ON groups_students (group_id)');
        $this->addSql('CREATE TABLE groups_teachers (user_id BIGINT NOT NULL, group_id BIGINT NOT NULL, PRIMARY KEY(user_id, group_id))');
        $this->addSql('CREATE INDEX IDX_5615E529A76ED395 ON groups_teachers (user_id)');
        $this->addSql('CREATE INDEX IDX_5615E529FE54D947 ON groups_teachers (group_id)');
        $this->addSql('ALTER TABLE skills_groups ADD CONSTRAINT skills_groups__skill_id__fk  FOREIGN KEY (skill_id) REFERENCES "skill" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE skills_groups ADD CONSTRAINT skills_groups__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE groups_students ADD CONSTRAINT groups_students__user_id__fk FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE groups_students ADD CONSTRAINT groups_students__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE groups_teachers ADD CONSTRAINT groups_teachers__user_id__fk FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE groups_teachers ADD CONSTRAINT groups_teachers__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD group_id BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT lesson__group_id__fk FOREIGN KEY (group_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F87474F3FE54D947 ON lesson (group_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "lesson" DROP CONSTRAINT lesson__group_id__fk');
        $this->addSql('ALTER TABLE skills_groups DROP CONSTRAINT skills_groups__group_id__fk');
        $this->addSql('ALTER TABLE skills_groups DROP CONSTRAINT skills_groups__skill_id__fk');
        $this->addSql('ALTER TABLE groups_students DROP CONSTRAINT groups_students__user_id__fk');
        $this->addSql('ALTER TABLE groups_students DROP CONSTRAINT groups_students__group_id__fk');
        $this->addSql('ALTER TABLE groups_teachers DROP CONSTRAINT groups_teachers__user_id__fk');
        $this->addSql('ALTER TABLE groups_teachers DROP CONSTRAINT groups_teachers__group_id__fk');
        $this->addSql('DROP TABLE "group"');
        $this->addSql('DROP TABLE skills_groups');
        $this->addSql('DROP TABLE groups_students');
        $this->addSql('DROP TABLE groups_teachers');
        $this->addSql('DROP INDEX IDX_F87474F3FE54D947');
        $this->addSql('ALTER TABLE "lesson" DROP group_id');
    }
}
